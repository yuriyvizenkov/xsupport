<?php

namespace XSupportBaseComponents;

use BaseInterfaces\IPagination;

/**
 * Class Pagination
 */
class Pagination implements IPagination
{

    /**
     * @var int
     */
    private $offset = 0;

    /**
     * @var int
     */
    private $page = 1;

    /**
     * @var int
     */
    private $countRows = 10;

    /**
     * @param int $page
     */
    public function __construct($page)
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getCountRows()
    {
        return $this->countRows;
    }
}
