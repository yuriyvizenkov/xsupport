<?php

/**
 * Class UserFactory
 */
class UserFactory 
{
	/**
	 * @param UserModel $userModel
	 * @return IUser
	 */
	public function getWebUser(UserModel $userModel)
	{
		return new WebUser($userModel);
	}
}
