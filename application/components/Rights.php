<?php

/**
 * Class Rights
 */
class Rights implements \BaseInterfaces\IRights
{

    /**
     * @var int []
     */
    private $layers = [1,2,3];

    /**
     * @return int[]
     */
    public function getLayers()
    {
        return $this->layers;
    }

    /**
     * @param array $layers
     */
    public function setRightLayers(array $layers)
    {
        array_merge($this->layers, $layers);
    }
}
