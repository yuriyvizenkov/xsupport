<?php

/**
 * Class WebUser
 */
class WebUser implements IUser
{
	/**
	 * @var bool|UserModel
	 */
	private $model = false;

	/**
	 * @var bool
	 */
	private $isGuest = true;

	/**
	 * @param UserModel $userModel
	 */
	public function __construct(UserModel $userModel)
	{
		$this->model = $userModel;
	}

	public function setAuthFlag()
	{
		$this->isGuest = false;
	}

	/**
	 * @return bool
	 */
	public function isGuest()
	{
		return $this->isGuest;
	}

	/**
	 * @return bool
	 */
	public function isAdmin()
	{
		// TODO: Implement isAdmin() method.
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->model->getLogin();
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->model->id;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		// TODO: Implement getStatus() method.
	}

	/**
	 * @return string
	 */
	public function getGroup()
	{
		// TODO: Implement getGroup() method.
	}

	/**
	 * @return string
	 */
	public function getLogin()
	{
		// TODO: Implement getLogin() method.
	}

	/**
	 * @return string
	 */
	public function getPhone()
	{
		// TODO: Implement getPhone() method.
	}

	/**
	 * @return string
	 */
	public function getEmail()
	{
		// TODO: Implement getEmail() method.
	}

    /**
     * @return Rights
     */
    public function getRights()
    {
        return new Rights();
    }
}
