<?php

namespace BaseInterfaces;

use CoreInterfaces\IComponent;

/**
 * Interface ICollection
 * @package BaseInterfaces
 */
interface ICollection extends IComponent {

}
