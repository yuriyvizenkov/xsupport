<?php

namespace BaseInterfaces;

/**
 * Interface IRights
 *
 * @package BaseInterfaces
 */
interface IRights {
    /**
     * @return int []
     */
    public function getLayers();

    /**
     * @param array $layers
     */
    public function setRightLayers(array $layers);
}
