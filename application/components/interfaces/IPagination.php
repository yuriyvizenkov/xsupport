<?php
/**
 * 
 */

namespace BaseInterfaces;

/**
 * Interface IPagination
 *
 * @package BaseInterfaces
 */
interface IPagination {
    /**
     * @param int $page
     */
    public function __construct($page);

    /**
     * @return int
     */
    public function getOffset();

    /**
     * @return int
     */
    public function getPage();

    /**
     * @return int
     */
    public function getCountRows();
}
