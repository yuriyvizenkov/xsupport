<?php

namespace BaseInterfaces;

use BaseModels\IThread;

/**
 * Interface IThreadsCollection
 *
 * @package BaseInterfaces
 */
interface IThreadsCollection
{
    /**
     * @param IThread[] $threads
     */
    public function __construct(array $threads);
}
