<?php
/**
 * 
 */

namespace BaseInterfaces;

/**
 * Interface ICategoryInfo
 *
 * @package BaseInterfaces
 */
interface ICategoryInfo {
    /**
     * @param string $categoryName
     * @return bool
     */
    public function isSelectCategory($categoryName);
}
