<?php

namespace BaseInterfaces;

use BaseModels\IMessage;

/**
 * Class IMessagesCollection
 * @package BaseInterfaces
 */
interface IMessagesCollection extends ICollection
{
    /**
     * @param bool|int $threadId
     * @return IMessage[]
     */
    public function getMessages($threadId = false);
}
