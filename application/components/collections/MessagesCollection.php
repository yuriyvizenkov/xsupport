<?php

namespace XSupportCollection;

use BaseInterfaces\IMessagesCollection;
use BaseModels\IMessage;
use XSupportCore\Component;

/**
 * Class MessagesCollection
 * @package XSupportCollections
 */
class MessagesCollection extends Component implements IMessagesCollection
{

    /**
     * @var array|IMessage[]
     */
    private $messages = [];

    /**
     * @param array $messagesModels
     */
    public function __construct(array $messagesModels)
    {
        $this->messages = $messagesModels;
    }

    /**
     * @param bool $threadId
     * @return array|IMessage[]
     */
    public function getMessages($threadId = false)
    {
        return $this->messages;
    }
}
