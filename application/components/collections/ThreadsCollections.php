<?php

namespace XSupportCollection;

use BaseInterfaces\IThreadsCollection;
use BaseModels\IThread;
use XSupportCore\Component;

/**
 * Class ThreadsCollections
 *
 * @package XSupportCollection
 */
class ThreadsCollections extends Component implements IThreadsCollection
{

    /**
     * @var IThread[]
     */
    private $threads = [];

    /**
     * @param IThread[] $threads
     */
    public function __construct(array $threads)
    {
        $this->threads = $threads;
    }

    /**
     * @return array|\BaseModels\IThread[]
     */
    public function getThreads()
    {
        return $this->threads;
    }
}
