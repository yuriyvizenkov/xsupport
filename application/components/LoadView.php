<?php
use BaseInterfaces\IMessagesCollection;

/**
 * Class LoadView
 */
class LoadView extends ViewModel
{

    /**
     * @var IMessagesCollection|null
     */
    private $messagesCollection;

    /**
     * @var \BaseModels\IProject[]|[]
     */
    private $projects = [];

    /**
     * @var \BaseModels\ISupporter[]
     */
    private $supporters = [];

    /**
     * @var \BaseModels\ISupporter
     */
    private $currentSupporter;

    /**
     * @var \BaseInterfaces\ICategoryInfo
     */
    private $baseMenuInfo;

    /**
     * @param IMessagesCollection $collection
     */
    public function setMessages(IMessagesCollection $collection)
    {
        $this->messagesCollection = $collection;
    }

    /**
     * @return array|\BaseModels\BaseInfoMessage[]
     */
    public function getMessages()
    {
        $messages = [];
        if ($this->messagesCollection instanceof IMessagesCollection) {
            $messages = $this->messagesCollection->getMessages();
        }

        return $messages;
    }

    /**
     * @param array $projects
     */
    public function setProjects(array $projects)
    {
        $this->projects = $projects;
    }

    /**
     * @return \BaseModels\IProject[]
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * @return \BaseModels\ISupporter[]
     */
    public function getSupporters()
    {
        return $this->supporters;
    }

    /**
     * @param \BaseModels\ISupporter[] $supporters
     */
    public function setSupporters(array $supporters)
    {
        $this->supporters = $supporters;
    }

    /**
     * @param \BaseModels\ISupporter $supporter
     */
    public function setCurrentSupporter(\BaseModels\ISupporter $supporter)
    {
        $this->currentSupporter = $supporter;
    }

    /**
     * @return \BaseModels\ISupporter
     */
    public function getCurrentSupporter()
    {
        return $this->currentSupporter;
    }

    /**
     * @return \BaseInterfaces\ICategoryInfo
     */
    public function getBaseMenuInfo()
    {
        return $this->baseMenuInfo;
    }

    /**
     * @param \BaseInterfaces\ICategoryInfo $baseMenuInfo
     */
    public function setBaseMenuInfo($baseMenuInfo)
    {
        $this->baseMenuInfo = $baseMenuInfo;
    }

    public function loadResource()
    {
        $this->clientManager->registerJS('core/underscore');
        $this->clientManager->registerJS('core/backbone');

        $this->clientManager->registerJS('core/app');

        $this->clientManager->registerJS('models/thread');
        $this->clientManager->registerJS('collection/threads');

        $this->clientManager->registerJS('views/threads');
        $this->clientManager->registerJS('views/menu');


        $this->clientManager->registerJS('load');
    }
}
