<?php
/**
 * @var $this Base_Loader
 * @var array $config
 */
?>
<div id="layout"></div>
<div class="footer_side">
    <div style="margin: 0px auto 0px auto; width: 450px;" id="block_auth">
        <blockquote class="rounded s">
            <div class="login-form-container">
                <div class="left-aligned logo">
                    <img src="/images/logo.png" alt=""/>
                </div>
                <div id="error" align="center"></div>
                <!--<form style="">-->
                <form onSubmit="return false;">
                    <div class="login-form">
                        <p>
                            <input type="edit" name="login" id="login"/>
                            <label for="login">Login</label>
                        </p>

                        <p>
                            <input type="password" name="password" id="password"/>
                            <label for="password">Password</label>
                        </p>

                        <p>
                            <input id="submit_auth" type="submit" value="Login"/>
                        </p>
                    </div>
                </form>
                <!--</form>-->
                <div class="clear"></div>
            </div>
        </blockquote>
    </div>
</div>

