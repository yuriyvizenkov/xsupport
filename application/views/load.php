<?php
/**
 * @var       $this Base_Loader
 * @var array $config
 * @var $view LoadView
 */
?>

<!--[if IE]>
<div class="IEWarning"><img src="/images/tw.png" alt="!"/>
    <b>Просим заметить</b>: Вы используете браузер на основе Microsoft Internet Explorer. Мы рекомендуем Вам
    использовать более современный браузеры, такие как <a href="http://www.opera.com/">Opera</a> или
    <a href="http://www.mozilla.org/">Firefox</a>.
    <img src="/images/tw.png" alt="!"/>
</div>
<![endif]-->

<div class="lcolumn">

    <div class="lc-elem logo">
        <a class="lc-elem" href="#">
            <img class="lc-elem" src="/images/logo.png">
        </a>
    </div>

    <?php widget('ProjectListWidget', ['projects' => $view->getProjects()]); ?>

    <button class="button orange" id="create_message"><?= t('создать сообщение') ?></button>

    <div class="base_menu">
        <!--include - menu.jstpl-->
        <?php widget('BaseMenuWidget', ['menuInfo' => $view->getBaseMenuInfo()]); ?>
    </div>
    <div class="tags_menu">
        <!--include - tags_menu.jstpl-->
        <?php widget('TagsMenuWidget'); ?>
    </div>

</div>

<div class="rcolumn">
    <div class="head">

        <div class="head-left-block">

			<span id="schedule"><?= t('График') ?>:
                <b>
                    <?= $view->getCurrentSupporter()->getSchedulePlan()->getStartDate(
                    ) ?> - <?= $view->getCurrentSupporter()->getSchedulePlan()->getEndDate() ?>
                </b>
            </span>

            <?php widget('SupportersListWidget', ['supporters' => $view->getSupporters()]); ?>

            <div class="time_server">
                <form name="time"><input type="text" size="12" name="result" id="time_server" onfocus="this.blur()"/>
                </form>
            </div>
        </div>

        <div class="head-right-block">
            <span class="lbtn option" id="btn_options" rel="#options_dialog">Настройки</span>
            <span class="lbtn exit" id="btn_exit">
                <?= t('Выход') ?>: <strong>[<?= getCurrentUser()->getName(); ?>]</strong>
            </span>

            <div class="lbtn_login">&nbsp;</div>
        </div>
    </div>

    <?php widget('SearchWidget'); ?>

    <div class="content">

        <div class="head-block">
            <?php widget('ControlsMessagesBlockWidget'); ?>
        </div>

        <div class="main-conteiner-loder-indicator" id="main-conteiner-loder-indicator-id">
            <?= t('Подождите идёт загрузка...') ?>
        </div>

        <div id="mainContainer">
            <table class="threds-list" id="threds-list">
                <tbody>
                <?php foreach ($view->getMessages() as $msg) : ?>
                    <tr>
                        <td class="checkbox">
                            <input type="checkbox" class="checkbox-input">
                        </td>
                        <td class="thread-mark-empty" style="">
                            <p class="star-p black" title=""></p>
                        </td>
                        <td class="from"><?= $msg->getFrom() ?></td>
                        <td class="tags-dayjest">
                            <div class="tags-inner">
                                <div class="tin-layer">
                                    <span class="tag-cloud"></span>
                                    <span class="tag-cloud"></span>
                                    <span class="tag-cloud"></span>
                                    <span class="title"><?= $msg->getSubject() ?></span>
                                    <span class="dayjest">
                                        <?= $msg->getShortDescriptionWithoutHtml(300) ?>
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td class="count">(<?= $msg->getCountInThread(); ?>)</td>
                        <td class="date"><?= $msg->getDataLastMsgInThread() ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <div class="conteiner-bottom-block">
            <?php widget('ControlsMessagesBlockWidget'); ?>
        </div>

    </div>

</div>
<input type="hidden" id="current_selected_filter" value="${selected_filter}"/>
<input type="hidden" id="type_search_for_pagging" value=""/>
<input type="hidden" id="text_search_for_pagging" value=""/>
<div id="tmpl-box"></div>



