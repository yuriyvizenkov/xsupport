<?php
/**
 * Description content file
 */
?>
<script id="tpl-thread" type="text/template">
    <tr>
    <td class="checkbox">
        <input type="checkbox" class="checkbox-input">
    </td>
    <td class="thread-mark-empty" style="">
        <p class="star-p black" title=""></p>
    </td>
    <td class="from"><%= model.from() %></td>
    <td class="tags-dayjest">
        <div class="tags-inner">
            <div class="tin-layer">
                <span class="tag-cloud"></span>
                <span class="tag-cloud"></span>
                <span class="tag-cloud"></span>
                <span class="title"><%= model.subject() %></span>
                <span class="dayjest">
                    <%= model.short_description() %>
                </span>
            </div>
        </div>
    </td>
    <td class="count">(<%= model.count_messages() %>)</td>
    <td class="date"><%= model.data_last() %></td>
</tr>
</script>