<?php
/**
 * @var $content string
 * @var $this Base_Loader
 * @var $clientManager ClientManager
 * @var $config array
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width">
    <meta name="keywords" content="<?= $clientManager->getMetaKeywords(); ?>">
    <meta name="description"
          content="<?= (isset($Description)) ? $Description : $clientManager->getDescription(); ?>">

    <title><?= $clientManager->getTitle(); ?></title>

    <?= $clientManager->getCanonical(); ?>

    <?php $clientManager->includeCSS(); ?>

    <?php $clientManager->registerCoreJQuery('core/jquery-1.10.2.min'); ?>
    <?php $clientManager->registerCoreJQuery('core/jquery-migrate-1.2.2.min'); ?>



    <!-- Include SpellChecker plugin -->
    <!--<script type="text/javascript" src="/.design/scripts/jquery/plugins/SpellChecker/jquery.spellchecker.js"></script>
    <link rel="stylesheet" href="/.design/scripts/jquery/plugins/SpellChecker/css/spellchecker.css" />-->
    <!-- End SpellChecker plugin -->

    <!--[if IE]>
    <link href='/.design/styles/ie.css' type='text/css' rel='stylesheet'/>
    <![endif]-->

    <style>
        /* SINGLE PROGRESS BAR */
        .progressBar {
            width: 216px;
            height: 41px;
            background: url(/.design/images/progress_bar/bg_bar.gif) no-repeat 0 0;
            position: relative;
        }

        .progressBar span {
            position: absolute;
            display: block;
            width: 200px;
            height: 25px;
            background: url(/.design/images/progress_bar/bar.gif) no-repeat 0 0;
            top: 8px;
            left: 8px;
            overflow: hidden;
            text-indent: -8000px;
        }

        .progressBar em {
            position: absolute;
            display: block;
            width: 200px;
            height: 15px;
            background: url(/.design/images/progress_bar/bg_cover.gif) repeat-x 0 0;
            top: 0;
        }

    </style>

</head>

<body class="body">

<span class="pb"></span>

<div class="bar"></div>

<div id="content">
    <?= $content; ?>
</div>

<div id="overlay"></div>

<?php $this->renderPartial('js_config', ['config' => $config]); ?>

<?php $clientManager->renderModalWindows(); ?>
<?php $clientManager->includeJS(); ?>

<script type="text/javascript">
    /*$(document).ready(function(){
        var date = new Date(),
            sep = '; ';
        jQuery.each(jQuery.browser, function(i, val) {
            $('body').prepend($('div').text(i + ': ' + val + sep));
        });
        $('body').prepend($('div').text('версия jQuery: ' + $().jquery + sep));
    });*/
</script>
</body>
</html>
