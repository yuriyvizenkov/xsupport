<?php

namespace XSupportCore;

use CoreInterfaces\IComponent;

/**
 * Class Component
 * @package XSupportCore
 */
abstract class Component implements IComponent
{
	public function init()
	{

	}
}
