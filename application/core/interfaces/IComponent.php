<?php

namespace CoreInterfaces;

/**
 * Interface IComponent
 * @package CoreInterfaces
 */
interface IComponent
{
	/**
	 * @return void
	 */
	public function init();
}
