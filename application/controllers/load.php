<?php

use BaseModels\ProjectModel;
use BaseModels\SupporterModel;

/**
 * Class controller load
 */
class load  extends Base_Controller
{
	public function index()
	{
        $this->getJsConfig()->set(['ext' => '.asc']);

        $this->clientManager->registerCSS('load');
        $this->getClientManager()->registerCSS('animate');

		if (isGuest()) {
            $this->clientManager->registerJS('core/app');
			$this->getClientManager()->registerJS('controller/auth');
			$this->render('login');
		}
		else {
			$view = $this->getView();

            $view->setMessages(
                new \XSupportCollection\MessagesCollection(
                    (new \XSupportModels\MessageModel())->findMessagesByUserAsThread(
                        $this->getCurrentUser(),
                        new \XSupportBaseComponents\Pagination($this->getPost('page'))
                    )
                )
            );

            $view->setProjects((new ProjectModel())->findProjects($this->getCurrentUser()));

            $view->setSupporters((new SupporterModel())->findSupporters());

            $view->setCurrentSupporter((new SupporterModel())->findById($this->getCurrentUser()->getId()));

            $view->setBaseMenuInfo(new BaseMenuInfo());

            $view->loadResource();

			$this->render('load', ['view' => $view]);
		}
	}

    /**
     * @param string $nameTmpl
     */
    public function loadTmpl($nameTmpl)
    {
        $this->renderPartial('templates_js/' . $nameTmpl);
    }

    /**
     * @return LoadView
     */
    private function getView()
    {
        return new LoadView();
    }
}
