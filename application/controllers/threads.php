<?php

use BaseModels\ThreadsSearchCriteria;

/**
 * Class controller load
 */
class threads extends Base_Controller
{
    /**
     * @param string $category
     * @param int $page
     */
    public function index($category, $page)
    {
        $error = [];
        $data = [];
        try {

            $criteria = new ThreadsSearchCriteria();
            $criteria->setPagination(new \XSupportBaseComponents\Pagination($page));
            $criteria->setUser($this->getCurrentUser());

            $data = (new \XSupportCollection\ThreadsCollections(
                (new \XSupportModels\ThreadModel())->findThreadsByUser($criteria)
            ))->getThreads();
        }
        catch (Exception $e) {
            $error[] = $e->getMessage();
        }

        $this->renderJson($data);
    }
}