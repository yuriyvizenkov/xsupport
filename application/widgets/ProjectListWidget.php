<?php

/**
 * Class ProjectListWidget
 */
class ProjectListWidget extends Widget
{
    /**
     * @var \BaseModels\IProject[]
     */
    public $projects = [];

    public function run()
    {
        $this->render('project_list', array('projects' => $this->projects));
    }

	/**
	 * @return bool
	 */
	public function isShow()
	{
		return true;
	}
}
