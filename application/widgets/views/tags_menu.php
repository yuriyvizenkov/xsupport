<?php
/**
 *
 */
?>
<ul class="left-menu-block s-menu">
{{each $data.tags}}
     {{! Замена вопросительного знака в аттрибуте, так как вываливается ошибка}}
     <li dir="${$value.name.replace('?', '')}"
     {{if $data.tag_id == $index}}class="selected"{{/if}}>
         <a class="menu" id="tag_${$value.tag_id}" name="${$value.name}">${$value.name} (${$value.count})</a></li>
 {{/each}}
</ul>