<?php
/**
 * @var $supporters \BaseModels\ISupporter[]
 */
?>
<select id="supporters">

    <?php foreach ($supporters as $supporter) : ?>
        <option value="<?= $supporter->getId(); ?>" <?php ($supporter->isActiveSupporter()) ? 'selected' : ''; ?>>
	        <?= $supporter->getLogin() ?>
	    </option>
    <?php endforeach; ?>

</select>