<?php
/**
 * @var $projects \BaseModels\IProject[]
 */
?>
<form action="" method="get" id="select_project_form">
	<!-- <input type="hidden" name="project_id" value="" /> -->
	<select name="project_id" id="change_project">
        <?php foreach ($projects as $project) : ?>
            <option value="<?= $project->getId(); ?>" <?= ($project->isCurrentProject()) ? ' selected' : ''; ?>>
                <?= $project->getName(); ?>
            </option>
        <?php endforeach; ?>
	</select>
</form>