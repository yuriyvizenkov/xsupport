<?php
/**
 * @var $isSearchRequest bool
 * @var $menuInfo \BaseInterfaces\ICategoryInfo
 * TODO: realization name category via enum object
 */
?>
<ul id="menu" class="left-menu-block f-menu">

    <li dir="myinbox" class="<?= ($menuInfo->isSelectCategory('myinbox') ? 'selected' : '') ?>">
        <a class="menu" id="myinbox" href="#load/myinbox">
            <?= t('Мои входящие') ?> (0)
        </a>
    </li>

    <li dir="mycustomers" class="<?= ($menuInfo->isSelectCategory('mycustomers') ? 'selected' : '') ?>">
        <a class="menu" id="mycustomers" href="#load/mycustomers">
            <?= t('Мои клиенты') ?> (0)
        </a>
    </li>

    <li dir="allinbox" class="<?= ($menuInfo->isSelectCategory('allinbox') ? 'selected' : '') ?>">
        <a class="menu" id="allinbox" href="#load/allinbox">
            <?= t('Все входящие') ?> (0)
        </a>
    </li>

    <li dir="mydrafts" class="<?= ($menuInfo->isSelectCategory('mydrafts') ? 'selected' : '') ?>">
        <a class="menu" id="mydrafts" href="#load/mydrafts">
            <?= t('Черновики') ?> (0)
        </a>
    </li>

    <li dir="problems" class="<?= ($menuInfo->isSelectCategory('problems') ? 'selected' : '') ?>">
        <a class="menu" id="problems" href="#load/problems">
            <?= t('Проблемные') ?> (0)
        </a>
    </li>

    <li dir="spam" class="<?= ($menuInfo->isSelectCategory('spam') ? 'selected' : '') ?>">
        <a class="menu" id="spam" href="#load/spam">
            <?= t('Спам') ?> (0)
        </a>
    </li>

    <?php if ($isSearchRequest === true) : ?>
        <li class="selected" dir="search">
            <?= t('Найдено') ?>&nbsp;(0)
        </li>
    <?php endif; ?>

</ul>