<?php

/**
 * Class SearchWidget
 */
class SearchWidget extends Widget
{
    public function run()
    {
        $this->render('search');
    }

	/**
	 * @return bool
	 */
	public function isShow()
	{
		return true;
	}
}
