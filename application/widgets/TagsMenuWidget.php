<?php

/**
 * Class TagsMenuWidget
 */
class TagsMenuWidget extends Widget
{
    /**
     * @var bool
     */
    public $menuInfo = false;

    public function init()
    {

    }

    public function run()
    {
        $this->render('tags_menu', array());
    }

    /**
     * @return bool
     */
    public function isShow()
    {
        return ($this->menuInfo === true);
    }
}