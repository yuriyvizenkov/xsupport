<?php

/**
 * Class BaseMenuWidget
 */
class BaseMenuWidget extends Widget
{
    /**
     * @var bool
     */
    public $isSearchRequest = false;

    /**
     * @var bool|\BaseInterfaces\ICategoryInfo
     */
    public $menuInfo = false;

    public function init()
    {

    }

    public function run()
    {
        $this->render(
            'base_menu',
            [
                'isSearchRequest' => $this->isSearchRequest,
                'menuInfo' => $this->menuInfo
            ]
        );
    }

    /**
     * @return bool
     */
    public function isShow()
    {
        return true;
    }
}