<?php

/**
 * Class ControlsMessagesBlockWidget
 */
class ControlsMessagesBlockWidget extends Widget
{
    public function init()
    {

    }

    public function run()
    {
        $this->render('controls_messages_block', []);
    }

	/**
	 * @return bool
	 */
	public function isShow()
	{
		return true;
	}
}