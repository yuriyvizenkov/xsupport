<?php

/**
 * Class SupportersListWidget
 */
class SupportersListWidget extends Widget
{
    /**
     * @var \BaseModels\ISupporter[]
     */
    public $supporters = [];

    public function run()
    {
        $this->render('supports_list', array('supporters' => $this->supporters));
    }

	/**
	 * @return bool
	 */
	public function isShow()
	{
		return true;
	}
}
