<?php

namespace BaseModels;

/**
 * Class ScheduleSupporterModel
 *
 * @package BaseModels
 */
class ScheduleSupporterModel extends \Base_Model implements IScheduleSupporter, IPeriod
{
	/**
	 * @var string
	 */
	protected $tableName = "e_support_schedule";

	/**
	 * @return string
	 */
	public function getTableName()
	{
		return $this->tableName;
	}

    /**
     * @return string
     */
    public function getStartDate()
    {

    }

    /**
     * @return string
     */
    public function getEndDate()
    {

    }
}