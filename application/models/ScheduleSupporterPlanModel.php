<?php

namespace BaseModels;

use DateInterval;
use DateTime;

/**
 * Class ScheduleSupporterPlanModel
 *
 * @property string $start_time
 * @property string $duration
 *
 * @package BaseModels
 */
class ScheduleSupporterPlanModel extends \Base_Model implements IScheduleSupporterPlan
{
	/**
	 * @var string
	 */
	protected $tableName = "e_support_schedule_plan";

	/**
	 * @return string
	 */
	public function getTableName()
	{
		return $this->tableName;
	}

    /**
     * @return string
     */
    public function getStartDate()
    {
        return (new DateTime($this->start_time))->format('Y-m-d H:i:s');
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        $start = new DateTime($this->getStartDate());
        $interval = new DateInterval($this->getFormatIntervalFromTime($this->duration));
        $start->add($interval);

        return $start->format("Y-m-d H:i:s");
    }

    /**
     * @param string $duration
     * @return string
     */
    private function getFormatIntervalFromTime($duration)
    {
        $partsDuration = explode(':', $duration);
        $interval = 'PT' . $partsDuration[0] . 'H' . $partsDuration[1] .'M' . $partsDuration[2] . 'S';

        return $interval;
    }

    /**
     * @param ISupporter $supporter
     * @return IScheduleSupporterPlan
     */
    public function findPlanBySupporter(ISupporter $supporter)
    {
        $this->db->where('t.support_id', $supporter->getId());

        return $this->find();
    }
}