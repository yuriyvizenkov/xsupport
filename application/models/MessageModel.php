<?php

namespace XSupportModels;

use BaseInterfaces\IPagination;
use BaseModels\IMessage;

/**
 * Class MessageModel
 *
 * @property string $from
 * @property string $subject
 * @property string $text
 * @property int $thread_id
 * @property string $date_received - TIMESTAMP
 * @property string $date_insert - TIMESTAMP
 *
 * not filed table
 * @property int $countMessagesInThread
 *
 * @package XSupportModels
 */
class MessageModel extends \Base_Model implements IMessage, \JsonSerializable
{
    const NEW_STATE = 'new';
    const DRAFT_STATE = 'draft';

	/**
	 * @var string
	 */
	protected $tableName = "e_messages";

	/**
	 * @return string
	 */
	public function getTableName()
	{
		return $this->tableName;
	}

    /**
     * @param \IUser $user
     * @param IPagination $page
     * @return MessageModel[]
     */
    public function findMessagesByUserAsThread(\IUser $user, IPagination $page)
    {
        $this->db->where_in('t.layer_id', $user->getRights()->getLayers());
        $this->db->where('t.layer_id!=', self::DRAFT_STATE);

        $this->db->limit($page->getCountRows(), $page->getOffset());
        $this->db->group_by('t.thread_id');

        $messages = $this->findAll();

        return $messages;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return int
     */
    public function getCountInThread()
    {
        $this->db->select('count(*) as countMessagesInThread');

        $this->db->where_in('t.thread_id', $this->thread_id);

        $r = $this->find();

        $count = 0;
        if ($r->countMessagesInThread) {
            $count = $r->countMessagesInThread;
        }

        return $count;
    }

    /**
     * @return string
     */
    public function getDataLastMsgInThread()
    {
        $this->db->select('t.date_insert');

        $this->db->where('t.thread_id', (int) $this->getThreadId());
        $this->db->order_by('t.date_insert', 'DESC');


        $messages = $this->findAll();

        $date = '';
        if (is_array($messages) && count($messages) > 0) {
            $msg = $messages[0];
            $dateTime = new \DateTime($msg->date_insert);
            $date = $dateTime->format('d M');
        }

        return $date;
    }

    /**
     * @return int
     */
    public function getThreadId()
    {
        return $this->thread_id;
    }

    /**
     * @param $maxLength
     * @param int $position
     * @param string $ellipsis
     * @return string
     */
    public function getShortDescriptionWithoutHtml($maxLength, $position = 1, $ellipsis = '&hellip;')
    {
        // Strip tags
		$str = trim(strip_tags($this->text));

		// Is the string long enough to ellipsize?
		if (strlen($str) <= $maxLength)
		{
			return $str;
		}

		$beg = substr($str, 0, floor($maxLength * $position));

		$position = ($position > 1) ? 1 : $position;

		if ($position === 1)
		{
			$end = substr($str, 0, -($maxLength - strlen($beg)));
		}
		else
		{
			$end = substr($str, -($maxLength - strlen($beg)));
		}

		return $beg.$ellipsis.$end;
    }

    /**
     * @return array
     */
    public function jsonSerialize ()
    {
        return [
            'from' => $this->getFrom(),
            'subject' => $this->getSubject(),
            'text' => $this->getShortDescriptionWithoutHtml(300)
        ];
    }
}
