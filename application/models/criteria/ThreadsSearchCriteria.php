<?php

namespace BaseModels;

use BaseInterfaces\IPagination;

/**
 * Class ThreadsSearchCriteria
 *
 * @package BaseModels
 */
class ThreadsSearchCriteria 
{

    /**
     * @var bool|\IUser
     */
    private $user = false;

    /**
     * @var bool|IPagination
     */
    private $pagination = false;

    /**
     * @return bool|\IUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \IUser $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return IPagination|bool
     */
    public function getPagination()
    {
        return $this->pagination;
    }

    /**
     * @param IPagination $pagination
     */
    public function setPagination(IPagination $pagination)
    {
        $this->pagination = $pagination;
    }
}
