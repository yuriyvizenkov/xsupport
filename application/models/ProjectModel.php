<?php

namespace BaseModels;

/**
 * Class ProjectModel
 *
 * @property integer $id
 * @property string $name
 *
 * @package BaseModels
 */
class ProjectModel extends \Base_Model implements IProject
{
    const ON_STATUS = 'on';
    const OFF_STATUS = 'off';

	/**
	 * @var string
	 */
	protected $tableName = "projects";

	/**
	 * @return string
	 */
	public function getTableName()
	{
		return $this->tableName;
	}

    /**
     * @param \IUser $user
     * @param string $status
     * @return ProjectModel[]
     */
    public function findProjects(\IUser $user, $status = self::ON_STATUS)
    {
        $this->db->where('t.status', $status);

        $projects = $this->findAll();

        return $projects;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * todo: set logic current project
     * @return bool
     */
    public function isCurrentProject()
    {
        return false;
    }
}
