<?php

namespace BaseModels;

/**
 * Class SupporterModel
 *
 * @property int|null $support_id
 * @property string $login
 *
 * @package BaseModels
 */
class SupporterModel  extends \Base_Model implements ISupporter
{
    const ACTIVE_STATE = 'active';
    const DISABLE_STATE = 'disable';

	/**
	 * @var string
	 */
	protected $tableName = "e_support";

	/**
	 * @return string
	 */
	public function getTableName()
	{
		return $this->tableName;
	}

    /**
     * @param string $status
     * @return ProjectModel[]
     */
    public function findSupporters( $status = self::ACTIVE_STATE)
    {
        $this->db->where('t.state', $status);

        $supporters = $this->findAll();

        return $supporters;
    }

    /**
     * @param int $id
     * @return ISupporter
     */
    public function findById($id)
    {
        $this->db->where('t.support_id', $id);

        return $this->find();
    }

    /**
     * @return IScheduleSupporterPlan
     */
    public function getSchedulePlan()
    {
        return (new ScheduleSupporterPlanModel())->findPlanBySupporter($this);
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->support_id;
    }

    /**
     * TODO: implement logic {{if $value.login== $data.login || $value.id== $data.replacement_id}}
     * @return bool
     */
    public function isActiveSupporter()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }
}
