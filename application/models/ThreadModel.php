<?php
namespace XSupportModels;

use BaseInterfaces\IPagination;
use BaseModels\IMessage;
use BaseModels\IThread;
use BaseModels\ThreadsSearchCriteria;

/**
 * Class ThreadModel
 *
 * @package BaseModels
 */
class ThreadModel implements IThread, \JsonSerializable
{

    /**
     * @var IMessage[]
     */
    private $messages = [];

    /**
     * @var string
     */
    protected $tableName = "e_threads";

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param ThreadsSearchCriteria $criteria
     * @return IMessage[]
     */
    public function findThreadsByUser(ThreadsSearchCriteria $criteria)
    {
        $threads = [];

        $messages = (new MessageModel())->findMessagesByUserAsThread($criteria->getUser(), $criteria->getPagination());
        foreach ($messages as $msg) {
            if (!isset($threads[$msg->getThreadId()])) {
                $threads[$msg->getThreadId()] = new ThreadModel();
            }

            $threads[$msg->getThreadId()]->setMessage($msg);
        }

        return $threads;
    }

    /**
     * @param IMessage[] $messages
     */
    public function setMessages(array $messages = [])
    {
        $this->messages = $messages;
    }

    /**
     * @param IMessage $message
     */
    public function setMessage(IMessage $message)
    {
        $this->messages[] = $message;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return (count($this->messages) > 0) ? $this->messages[0]->from : '';
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return (count($this->messages) > 0) ? $this->messages[0]->subject : '';
    }

    /**
     * @return int
     */
    public function getCountInThread()
    {
        return (new MessageModel())->getCountInThread();
    }

    /**
     * @return string
     */
    public function getDataLastMsgInThread()
    {
        return (count($this->messages) > 0) ? $this->messages[0]->getDataLastMsgInThread() : '';
    }

    /**
     * @return int
     */
    public function getThreadId()
    {
        return (count($this->messages) > 0) ? $this->messages[0]->thread_id : false;
    }

    /**
     * @param $maxLength
     * @param int $position
     * @param string $ellipsis
     * @return string
     */
    public function getShortDescriptionWithoutHtml($maxLength, $position = 1, $ellipsis = '&hellip;')
    {
        return (count($this->messages) > 0)
            ? $this->messages[0]->getShortDescriptionWithoutHtml($maxLength, $position, $ellipsis) : '';
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'from' => $this->getFrom(),
            'subject' => $this->getSubject(),
            'short_description' => $this->getShortDescriptionWithoutHtml(300),
            'id' => $this->getThreadId(),
            'count_messages_in_thread' => $this->getCountInThread(),
            'data_last_message' => $this->getDataLastMsgInThread()
        ];
    }
}
