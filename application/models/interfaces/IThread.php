<?php

namespace BaseModels;

/**
 * Interface IThread
 *
 * @package BaseModels
 */
interface IThread extends BaseInfoMessage{
    /**
     * @param ThreadsSearchCriteria $criteria
     * @return IThread[]
     */
    public function findThreadsByUser(ThreadsSearchCriteria $criteria);

    /**
     * @param IMessage[] $messages
     */
    public function setMessages(array $messages = []);

    /**
     * @param IMessage $message
     */
    public function setMessage(IMessage $message);
}
