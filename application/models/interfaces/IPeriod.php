<?php

namespace BaseModels;

/**
 * Interface IPeriod
 *
 * @package BaseModels
 */
interface IPeriod {
    /**
     * @return string
     */
    public function getStartDate();

    /**
     * @return string
     */
    public function getEndDate();
}
