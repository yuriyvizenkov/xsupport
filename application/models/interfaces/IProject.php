<?php

namespace BaseModels;

/**
 * Interface IProject
 *
 * @package BaseModels
 */
interface IProject
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return bool
     */
    public function isCurrentProject();
}
