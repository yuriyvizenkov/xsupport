<?php

namespace BaseModels;

/**
 * Interface BaseInfoMessage
 *
 * @package BaseModels
 */
interface BaseInfoMessage
{
    /**
     * @return string
     */
    public function getFrom();

    /**
     * @return string
     */
    public function getSubject();

    /**
     * @param $maxLength
     * @param int $position
     * @param string $ellipsis
     * @return string
     */
    public function getShortDescriptionWithoutHtml($maxLength, $position = 1, $ellipsis = '&hellip;');

    /**
     * @return int
     */
    public function getCountInThread();

    /**
     * @return string
     */
    public function getDataLastMsgInThread();

    /**
     * @return int
     */
    public function getThreadId();
}
