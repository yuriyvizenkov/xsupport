<?php

namespace BaseModels;

/**
 * Interface ISupporter
 *
 * @package BaseModels
 */
interface ISupporter {
    /**
     * @return int|null
     */
    public function getId();

    /**
     * @return bool
     */
    public function isActiveSupporter();

    /**
     * @return string
     */
    public function getLogin();

    /**
     * @return IScheduleSupporterPlan
     */
    public function getSchedulePlan();
}
