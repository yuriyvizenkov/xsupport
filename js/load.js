﻿/*var Controller = Backbone.Router.extend({
    routes: {
        "": "main", // Пустой hash-тэг
        "!/": "main", // Начальная страница
        "!/success": "success", // Блок удачи
        "!/error": "error" // Блок ошибки
    },

    start: function () {
        $(".block").hide(); // Прячем все блоки
        $("#start").show(); // Показываем нужный
    },

    success: function () {
        $(".block").hide();
        $("#success").show();
    },

    error: function () {
        $(".block").hide();
        $("#error").show();
    }
});

var controller = new Controller(); // Создаём контроллер

Backbone.history.start();  // Запускаем HTML5 History push

var Main = Backbone.View.extend({
    el: $("#start"), // DOM элемент widget'а
    events: {
        "click input:button": "check" // Обработчик клика на кнопке "Проверить"
    },
    check: function () {
        if (this.el.find("input:text").val() == "test") // Проверка текста
            controller.navigate("success", true); // переход на страницу success
        else
            controller.navigate("error", true); // переход на страницу error
    }
});

var main = new Main();*/

$(document).ready(function(){
    $.app.init();

    var Router = Backbone.Router.extend({
        routes: {
            "threads/:category/:page": "threads",
            "threads": "threads",
            "load/:category": "base_menu",
            //"menu": "base_menu",
            "": "list"
        },
        list: function(archive) {
        },
        threads: function(category, page) {
            $.app.viewsFactory.threads().changeList(category, page);
        },
        base_menu: function(category) {
            this.threads(category, 1);
        }

    });

    $.app.router = new Router();

    Backbone.history.start();
});