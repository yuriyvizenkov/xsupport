$.app.models.ThreadModel = Backbone.Model.extend({
    defaults: {
        id: false,
        from: false,
        subject: false,
        text: false
    },
    from: function() {
        return this.get('from');
    },
    subject: function() {
        return this.get('subject');
    },
    short_description: function(){
        return this.get('short_description');
    },
    count_messages: function() {
        return this.get('count_messages_in_thread');
    },
    data_last: function(){
        return this.get('data_last_message');
    }
});
