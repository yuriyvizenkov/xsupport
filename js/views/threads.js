$.app.views.threads = Backbone.View.extend({
    initialize: function() {
        var self = this;

        $.app.loadTmpl(
            "thread",
            {},
            function() {
                self.template = _.template($("#tpl-thread").html());
            }
        );

        this.collection = $.app.collectionFactory.threads();

        /*var handler = _.bind(this.render, this);

         this.model.bind('change', handler);
         this.model.bind('add', handler);
         this.model.bind('remove', handler);*/
    },
    changeList: function(category, page) {
        var that = this;
        this.collection.reload({}, category + "/" + page, function(collection, r) {
            that.render();
        });
    },
    render: function() {
        this.$el.empty();
        this.collection.each(this.addOne, this);
        return this;
    },
    addOne: function(threadModel) {
        this.$el.append(this.template({model: threadModel}));
    }
});
