function log(value, msg) {
    if (msg)
        console.log(value, msg);
    else
        console.log(value);
}

var l = log;

/**
 * Отладочная функция, аналог var_dump в PHP
 * @param obj
 */
function dump(obj) {
    var out = "";
    if (obj && typeof(obj) == "object") {
        for (var i in obj) {
            out += i + ": " + obj[i] + "\r\n";
        }
    } else {
        out = obj;
    }
    alert(out);
}

(function($) {
    var config = $.conf;

    if (config.debug === true) {
        console.log(config);
    }

    $.app = {
        views: {},
        models: {},
        collections: {},
        content: null,
        router: null,
        todos: null,
        viewsFactory: {
            threadsView: false,
            menu: function() {
                if (!this.menuView) {
                    this.menuView = new $.app.views.menu({
                        el: $("#menu")
                    });
                }
                return this.menuView;
            },
            threads: function() {
                if (!this.threadsView) {
                    this.threadsView = new $.app.views.threads({
                        el: $("#threds-list")
                    });
                }
                return this.threadsView;
            }
        },
        collectionFactory: {
            threads: function() {
                if (!this.threadsCollection) {
                    this.threadsCollection = new $.app.collections.Threads();
                }
                return this.threadsCollection;
            }
        },
        init: function() {
            //this.content = $("#content");
            //this.threads = new this.collections.Threads();
            var menu = this.viewsFactory.menu();
            var threads = this.viewsFactory.threads();
        },
        changeContent: function(el) {
            this.content.empty().append(el);
            return this;
        },
        title: function(str) {
            $("h1").text(str);
            return this;
        },
        /**
         *
         * @param param
         * @returns {*}
         */
        get: function(param) {
            var v = null;
            if (typeof config[param] != 'undefined') {
                v = config[param];
            }

            return v;
        },
        /**
         * Переопределение метода запроса (синхронизации) с сервером.
         *
         * @param method {string}
         * @param model {Object}
         * @param options {Object}
         */
        sync: function(method, model, options) {

            var data = prepareRequestData(method, model, options);
            var xhr = $.ajax({
                async: (options.async) ? options.async : true,
                type: 'POST',
                url: (options.url) ? options.url : model.url,
                data: data,
                dataType: 'json',
                success: function(data) {
                    if (data !== null) {
                        if (typeof data.error === 'string') {
                            if (typeof options.error == 'function') options.error(data.error);

                            return false;
                        }

                        options.success(data.rows ? data.rows : data);
                    }

                    return true;
                },
                error: function(req_obj, msg, error) {
                    log('Error request: "' + req_obj.responseText + '"');
                    if (typeof options.error == 'function') options.error(req_obj);
                }
            });

            /**
             * Подготовка данных для отправки на сервер.
             *
             * @param method {string}
             * @param model {Object}
             * @param options {Object}
             * @return {Object}
             */
            function prepareRequestData(method, model, options) {
                var data = {action: method};
                if (model.attributes) $.extend(data, model.attributes);
                if (!options.data) options.data = {};
                $.extend(data, options.data);

                var cleanData = {};
                $.each(data, function(i, value) {
                    if (typeof data[i] != 'function') cleanData[i] = value;
                });

                return cleanData;
            }
        },
        redirect: function(url) {
            document.location.href = url;
        },
        setCSS: function(url, media) {
            $(document.createElement('link')).attr({
                href: url,
                media: media || 'screen',
                type: 'text/css',
                rel: 'stylesheet'
            }).appendTo('head');
        },
        loadTmpl: function(tmplName, data, callback) {
            var tmplBox = $('#tmpl-box');

            if (typeof callback == 'undefined') {
                callback = function(){};
            }
            if (typeof data == 'undefined') {
                data = {};
            }

            if (tmplBox.find('#' + tmplName).length === 0) {
                tmplBox.load('load/tmpl/' + tmplName, data, callback);
            }
        }
    };

    return $.app;

})(jQuery);


