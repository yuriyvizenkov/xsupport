/**
 * Author: Greg
 * Date: 13.04.11
 * To change this template use File | Settings | File Templates.
 */
(function($)
{
    $.conf = {
        ext : '.html',
        support : '',
        PAGINATION_THREADS_PER_PAGE  : 30,
        PAGINATION_MESSAGES_PER_PAGE : 30,
        //boolLoadRedactor : false,
        historyInit : false,
        inputFromUrl : false,
        debug : true,
        time_save_draft : 30000,
        server_hours : false,
        difference_hours : false,
        u_cfg : false
    };
})(jQuery);
