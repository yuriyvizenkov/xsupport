$.app.collections.Threads = Backbone.Collection.extend({
    extUrl: false,
    url: function(method) {
        return (this.extUrl) ? '/threads/' + this.extUrl : '/threads/';
    },
    model: $.app.models.ThreadModel,
    initialize: function() {},
    up: function(index) {
        if (index > 0) {
            var tmp = this.models[index - 1];
            this.models[index - 1] = this.models[index];
            this.models[index] = tmp;
            this.trigger("change");
        }
    },
    down: function(index) {
        if (index < this.models.length - 1) {
            var tmp = this.models[index + 1];
            this.models[index + 1] = this.models[index];
            this.models[index] = tmp;
            this.trigger("change");
        }
    },
    archive: function(archived, index) {
        this.models[index].set("archived", archived);
    },
    changeStatus: function(done, index) {
        this.models[index].set("done", done);
    },
    reload: function(data, url, callback) {
        var that = this;
        this.extUrl = (url) ? url : '';

        var options = ({
            data: (typeof data == 'undefined') ? {} : data,
            //method: 'post',
            error: function(r) {
                log('Ошибка обновления записей!', r);
                that.trigger('change');
            },
            success: function(r) {
                that.trigger('change');
                if (typeof callback == 'function') {
                    callback(that, r);
                }
            }
        });

        this.fetch(options);
    }
});
