/**
 * Author: Lenfer
 * Date: 01.06.11
 * Компонент реализующий пейджинг кортежей и сообщений.
 */
function gmLikePagging(_config){
	var $this = this
	function _prev(){
		if($this._config.currentPage==1) return;
		return _gotoPage($this._config.currentPage-1)
	}
	function _next(){
		if($this._config.currentPage==$this._config.pageCnt) return;
		return _gotoPage($this._config.currentPage+1)
	}
	function _gotoPage(page)
    {
		var data = {
			filter : $this._config.filter,
			start : (page-1)*$this._config.recOnPage,
			page : page,
			count : $this._config.recOnPage,
			id : $this._config.id
		}

        // Если выбран вывод списка тредов по событию "Поиск"
        var data_search = {};
        if($this._config.filter == 'search')
        {
            var type_search = $('#type_search_for_pagging').val();
            var text_serch = $('#text_search_for_pagging').val();
            data_search = {
                type_search : type_search,
                text : text_serch
            };
        }

        // Слияние отправляемых данных
        $.extend(data, data_search);
		
		//console.log('goto %s page', data.page)
		CompositeController.RUN_EVENT_HISTORY({
			Thread: {
				action: 'getThreadsForPagination',
				cache: true,
				data: data
			}
		});
		return false;
	}
	//Объявление дефолтного конфига
	this._config = {
		renderTo: 'body', 
		currentPage: 2,
		recOnPage: 50, 
		recsCnt: 510,
		filter: 'myinbox', 
		id: 'gmLikePaggingID'
	}
	// Слияние дефотного и переданного при создании
	$.extend(this._config, _config)
	// Объявление методов класса
	$.extend(this,{
		gmlpRender: function(rplace){
			var 
				ctrl = $('<div class="gmlpc-ctrl"></div>'), 
				prev = $('<span class="gmlpc-elem prev"></span>'), 
				next = $('<span class="gmlpc-elem next"></span>'), 
				current = $('<span class="gmlpc-current"></span>'), 
				select = $('<select class="gmlpc-select"></select>')
			// собираем контрол перехода влево
			prev.append(
				
				$('<a></a>').attr({
					'class': 'gmlpc-href gi', 
					'href': '#', 
					'title': 'Перейти на первую страницу'
				})
				.html('первая')
				.click(function(){return _gotoPage(1)}), 
				
				$('<a></a>').attr({
					'class': 'gmlpc-href', 
					'href': '#', 
					'title': 'Ctrl+влево'
				})
				.html('<<')
				.click(_prev) 
				
			)
			
			// собираем контрол перехода вправо
			next.append(
				$('<a></a>').attr({
					'class': 'gmlpc-href', 
					'href': '#', 
					'title': 'Ctrl+вправо'
				})
				.html('>>')
				.click(_next), 
				
				$('<a></a>').attr({
					'class': 'gmlpc-href gi', 
					'href': '#', 
					'title': 'Перейти на последнюю страницу'
				})
				.html('последняя')
				.click(function(){return _gotoPage($this._config.pageCnt)})
				
			)
			// Считаем и создаем контрол текущего полоположения
			var curStr = '';
			// Рендерим произвольный селект страницы
			for(var i=1; i<=this._config.pageCnt; i++){
				var opt = $('<option value="'+i+'">'+i+'</option>')
				if (i == this._config.currentPage) {
					//console.log('page %s selected', i)
					opt.attr('selected', 'selected')
				}
				select.append(opt) 
			}
			
			select.change(function(){
				_gotoPage($(this).val())
			})
			
			// применяем логику отображений в зависимости от настроект пагинации 
			with(this._config){
				// set next & prev href block
				currentPage == 1 ? prev.hide():''
				currentPage == pageCnt ? next.hide() : ''
				// render current page string
				var curStr = ''
				curStr += recsCnt ? (currentPage-1)*recOnPage+1 : '0' 
				curStr += ' - '
				curStr += recsCnt < recOnPage*currentPage ? recsCnt : (currentPage)*recOnPage
				curStr += ' из '
				curStr += recsCnt
				current.html(curStr)
				// hide if need select block
				recsCnt <= recOnPage ? select.hide() : ''
			}
			
			
			// Собираем контрол
			ctrl.append(prev,current, select, next) 
			//$.extend(this, ctrl);
			rplace.append(ctrl)
		}
	})
	this._config.pageCnt = Math.ceil(this._config.recsCnt/this._config.recOnPage)
	this._config.pageCnt = this._config.pageCnt == 0 ? 1 : this._config.pageCnt 
	
	var isCtrl = false; 
	$(document)
		.keyup(function (e) { 
			if(e.which == 17) isCtrl=false; 
		})
		.keydown(function (e) { 
			if(e.which == 17) isCtrl=true; 
			// right
			if(e.which == 39 && isCtrl == true) _next()
			// left
			if(e.which == 37 && isCtrl == true) _prev()
		});
	$(this._config.renderTo).each(function(){
		$this.gmlpRender($(this));	
	})
	
	return this;
}


function gmLikePaggingMsg(_config){
	var $this = this
	function _prev(){
		//if($this._config.currentPage==1) return;
		return _gotoPage(0)
	}
	function _next(){
		//if($this._config.currentPage==$this._config.pageCnt) return;
		return _gotoPage(0)
	}
	function _gotoPage(page){
		/*var data = {
			filter : $this._config.filter,
			start : (page-1)*$this._config.recOnPage,
			page : page,
			count : $this._config.recOnPage,
			id : $this._config.id
		}*/
		
		//console.log('goto %s page', data.page)
		
		/*CompositeController.RUN_EVENT_HISTORY({
			Thread: {
				action: 'getThreadsForPagination',
				cache: true,
				data: data
			}
		});*/
		return false;
	}
	//Объявление дефолтного конфига
	this._config = {
		renderTo: 'body', 
		currentThread: 123,
		//threadsCnt: 510,
		//filter: 'myinbox', 
		//id: 'gmLikePaggingID'
	}
	// Слияние дефотного и переданного при создании
	$.extend(this._config, _config)
	// Объявление методов класса
	$.extend(this,{
		gmlpRender: function(rplace){
			var 
				ctrl = $('<div class="gmlpc-ctrl"></div>'), 
				prev = $('<span class="gmlpc-elem prev"></span>'), 
				next = $('<span class="gmlpc-elem next"></span>'), 
				current = $('<span class="gmlpc-current"></span>')
			// собираем контрол перехода влево
			prev.append(
				$('<a></a>').attr({
					'class': 'gmlpc-href', 
					'href': '#', 
					'title': 'Ctrl+влево'
				})
				.html('<<пред.')
				.click(_prev) 
			)
			
			// собираем контрол перехода вправо
			next.append(
				$('<a></a>').attr({
					'class': 'gmlpc-href', 
					'href': '#', 
					'title': 'Ctrl+вправо'
				})
				.html('след.>>')
				.click(_next)
			)
			// Считаем текущего полоположения
			current.html('Сообщения из треда: ' + $this._config.currentThread)
			// Собираем контрол
			ctrl.append(prev,current, next) 
			//$.extend(this, ctrl);
			rplace.append(ctrl)
		}
	})
	var isCtrl = false; 
	$(document)
		.keyup(function (e) { 
			if(e.which == 17) isCtrl=false; 
		})
		.keydown(function (e) { 
			if(e.which == 17) isCtrl=true; 
			// right
			if(e.which == 39 && isCtrl == true) _next()
			// left
			if(e.which == 37 && isCtrl == true) _prev()
		});
	$(this._config.renderTo).each(function(){
		$this.gmlpRender($(this));	
	})
	return this;
}

/*
 $('.pagination').live("click", function(event)
    {
        var id = $(this).attr('id');
        var arr  = id.split('_');
        var page = $(this).val();

        var data = {
            filter : arr[0],
            start : arr[1],
            page : page,
            count : $.conf['PAGINATION_THREADS_PER_PAGE'],
            id : id
        }

        var pagination = CompositeController.RUN_EVENT_HISTORY({
            Thread: {
                action: 'getThreadsForPagination',
                cache: true,
                data: data
            }
        });

        return false;
    });
 */