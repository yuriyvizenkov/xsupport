/**
 * @class answerMsgCtrl
 * Это класс контрол панели ответа
 * <p>Пример:</p>
 * <pre><code>
 * new
 * </code></pre>
 * @constructor
 * Создать новый объект.
 * @param {Object} _config объект настроек.
 * @return {answerMsgCtrl} возвращает объект
 */
var _l = console.log
function answerMsgCtrl(config, _conf)
{
    this.blocks = {}

    /**
     * Флаг, указывающий что есть не сохранённые данные в теле редактора
     * @var bool
     */
    this.not_save = false;

    /**
     * Индивидуальные настройки окна
     * @var object
     */
    this.config = config;

    /**
     * Данные сообщения от сервера
     * @var object
     */
    this.conf = (typeof _conf != 'undefined') ? _conf : {};

    /**
     * Слои для рендеринга markList (вкладок окна ответа)
     * @var object
     */
    this.layers = (typeof _conf['layers'] != 'undefined') ? _conf['layers'] : {};
    

    /**
     * Контекст окна ответа:
     * 1. Для ответа на сообщение
     * 2. Для нового сообщения
     * @var string
     */
    this.type_answer = (typeof _conf['type_answer'] != 'undefined') ? _conf['type_answer'] : 'answer';

    /**
     * Идентификатор редактора
     * @var string
     */
    this.editorID = 'editorID' + ~(new Date())

    /**
     * Контекст вывода окна ответа "Редактировать" или "Отправить"
     * @var string
     */
    this.edit = _conf['editType'];

    /**
     * Слой, который выбран пользователем вкладки
     */
    this.reply_layer_id = config.reply_layer_id;

    /**
     * Идентификатор сообщения
     * Если for_ms = 0 сообщение является новым
     * @var int
     */
    this.msg_id = (_conf.for_msg != 0) ? _conf.for_msg : _conf.msg_id;

    /**
     * Идентификатор треда
     * @var int
     */
    this.thread_id = _conf.thread_id;

    /**
     * Идентификатор нового сообщения, которое создаётся при клике "Ответить"
     * При клике "Редактировать" оно будет равно msg_id, и установлено по  умолчанию
     * @var int
     */
    this.reply_msg_id = _conf.msg_id;

    /**
     * Значение поля "Кому"
     * @var string
     */
    this.reply_to = _conf.to;

    /**
     * Значение поля text
     * @var string (html)
     */
    this.msgtext = _conf.text;
    console.log(this.msgtext)

    /**
     * Флаг, указывающий что в редакторе уже был произведён ввод символов
     * @var bool
     */
    this.keyShot = false;

    /**
     * Объект инициализации редактора
     * @var bool|object
     */
    this._editor = false;

    /**
     * Идентификатор запуска функции setInterval для авто сохранения черновика в фоне
     * @var int
     */
    this.draftIntervalID = false;

    /**
     * Флаг нового окна
     * @var bool
     */
    this.new_window = true;

    /**
     * Выделение окна взависимости от типа сообщения
     * Свойство сохраняет в себе формат цвета вида example: #FFFFFF
     * @var bool|string
     */
    this.background_color = false;

    /**
     * Доступ к самому объекту (родителю) во всём классе
     * @var object
     */
    var $this = this;

    /**
     * Интерфейс для очистки фоновых процессов и сохранения контента редактора
     * @return void
     */
    this.clearProcessAnswerWithSave = function()
    {
        var type = $this.conf['state'];
        _clearProcess(type);
    }

    /**
     * Интерфейс возвращает число от функции setInterval от метода _autoSave() для завершения фонового процесса
     * Служит для отдельного завершения фонового процесса от комплексных операций типа с сохраением контента
     * @return int
     */
    this.getIntervalID = function()
    {
        return $this.draftIntervalID;
    }

    /**
     * Интерфейс служит для изменения состояния свойства контента редактора (изменён - не изменён)
     * @param bool
     */
    this.setSaveFlag = function(status)
    {
        $this.not_save = status;
    }

    /**
     * Приватная функция инкапсулирующая алгоритм сохранения контента окна ответа взависимости от типа события,
     * так же включает в себя остановку фоновых процессов
     * @param obj|string
     */
    function _clearProcess(objData)
    {
        // Данные переданные из события, тип события
        // Удаление, В черновик и Отправить (del_draft, draft, send соответственно)
        var type = (objData.data) ? objData.data : objData;

        if(type == 'send')
        {
            _eventSendAnswer('send');
        }
        else if(type == 'draft')
        {
            _eventSendAnswer('draft');
        }

        console.log('autosave disabled to msg_id: ' + $this.reply_msg_id);
        clearInterval($this.draftIntervalID);
    }

    /**
     * Реакция на клик по контролам "Отправить", "В черновик" и "Удалить"
     * так же используется "Автосохранением"
     * @param objData
     */
    function _eventSendAnswer(objData)
    {
        // Данные переданные из события, тип события
        // Удаление, В черновик и Отправить (del_draft, draft, send соответственно)
        var type = (objData.data) ? objData.data : objData;

        // Если было выбрано удаление, спрашиваем подтверждение операции
        // TODO: Вынести удаление в отдельный обработчик!!!
        if (type == 'del_draft') {
            if (!confirm("Вы уверены, что хотите удалить этот черновик?")) {
                return false;
            }
        }

        if(type != 'del_draft')
        {
            var text = $this._editor.getHtml();
            var to = $('#to_' + $this.editorID).val();
            if(typeof to == 'undefined' || to == '' || to == ' ')
            {
                alert('Field `to` must be not empty!');
                return false;
            }
        }


        var msg = CompositeController.RUN_EVENT({
            Answer: {
                action : 'sendAnswer',
                data : {
                    ctrlAnswer : $this,
                    'get_thread' : ($this.type_answer == 'new_thread') ? 'true' : '',
                    // Тип запроса
                    'typeAnswer' : type,
                    // тело ответа (если удаление тогда не требуется текста)
                    'text' : (type != 'del_draft') ? text : '',
                    // идентификатор слоя для ответа
                    'layer_id' : $this.reply_layer_id,
                    // кому
                    'to' : to,
                    // идентификатор сообщения В КОТОРОЕ БУДУТ СОХРАНЯТЬСЯ ДАННЫЕ
                    // если контекст "Редактирование" reply_msg_id будет равен msg_id
                    // если контекст "Отправить" reply_msg_id будет равен вновь созданному сообщению на сервере
                    // при клике "Ответить" или "Редактировать"
                    'reply_msg_id' : $this.reply_msg_id,
                    // идентификатор сообщения определённого с сервера
                    'msg_id' : $this.reply_msg_id,
                    // идентификатор треда для сохраняемого сообщения
                    'thread_id' : $this.thread_id,
                    // Данные для возврата списка тредов
                    'filter' : $('#current_selected_filter').val(),
                    'count' : $.conf['PAGINATION_THREADS_PER_PAGE']
                }
            }
        });

        // Выводим уведомление об успешном сохранении в черновик 
        if (type == 'draft')
        {
            $('.mac-marks').append('<div class="mac-marks-save-draft">Сохранено в черновик</div>');
            setTimeout(function()
            {
                $('.mac-marks-save-draft').remove();
            }, 3000);
        }
        else
        {
            // Фокусировка на верхнем блоке, для возвращения положения центра страницы в исходное состояние
            // Сделано из за того что при перезагрузке списка не поднимается фокус к верху страницы и не видно контента
            $('.head-block').focus();
        }

        // Устанавливаем флаг сохранения изменений из редактора
        $this.not_save = false;
    }

     /**
     * Cобытия переключения вкладки (изменение слоя)
     * Применяется для того что бы определить доступное сообщение по выбранному слою, т.е.
     * поля текст и кому и изменять их в данном окне ответа
     */
    function _changeRequestLayer()
    {
        var msg = CompositeController.RUN_EVENT({
            Answer: {
                action : 'changeLayer',
                data : {
                    ctrlAnswer : $this,
                    // определение набора хотя бы одного символа, если был ввод текст на сервере не определяется
                    // определяется только поле кому
                    'keyShot' : $this.keyShot,
                    // идентификатор слоя для ответа
                    'reply_layer_id' : $this.reply_layer_id,
                    'thread_id' : $this.thread_id,
                    // Если выбирается слой для кастомера выбираем идентификатор сообщения для ответа
                    // Если выбираются остальные слои (системные) берём идентификатор своего сообщения - черновика
                    // Оба идентификатора сообщения равны только тогда, когда контекст окна ответа в "edit" (Редактировать)
                    'msg_id' : $this.msg_id,
                    'reply_msg_id' : $this.reply_msg_id ,
                    'edit'   : $this.edit
                }
            }
        });
    }

    function _autoSave()
    {
        $this.draftIntervalID = setInterval(function()
        {
            _eventSendAnswer('draft');
        }, $.conf['time_save_draft'])
        return $this.draftIntervalID;
    }

    /**
     * Инициализация каркаса
     */
    function _initLayout()
    {
        // Добавляем атачи
        _this.blocks.attach = $('<div>').attr('id', 'attachID' + _this.editorID).addClass('mac-attachs')
                .append($('<div>').addClass('qq-upload-button').html('Upload a file')
                    .append($('<input>').attr('type', 'file').attr('name', 'myfile').attr('class', 'upload_file')));

        // Инициализируем каркас окна ответа для определённого сообщения
        _layout = $('<table>').addClass('mac-tbl-merka').html($('<tr>').append(
                $('<td>').append(
                        $('<div>').addClass('mac-top-btn').append(
                                $('<button>').addClass('button orange').html('Отправить').attr('data-name', 'send').click('send', _eventSendAnswer),
                                $('<button>').addClass('button grey dark').html('В черновик').attr('data-name', 'indraft').click('draft', _eventSendAnswer),
                                $('<button>').addClass('button grey dark').html('Удалить').attr('data-name', 'del_draft').click('del_draft', _eventSendAnswer),
                                $('<button>').addClass('button grey dark').html('Метки').attr('data-name', 'mark')
                                ),
                        $('<label>').addClass('mac-label').html('Кому:'),
                        _this.blocks.fortext = $('<input>').attr('type', 'text').addClass('mac-fortext').attr('id', 'to_' + $this.editorID).val($this.reply_to),
                        _this.blocks.attachBlock = $('<div>').addClass('mac-attach-block').html(_this.blocks.attach),
                        _this.blocks.attachBlockComplete = $('<div>').addClass('mac-attach-block-complete'),
                        // Добавление текста в textarea
                        _this.blocks.msgtext = $('<' + config.textareaType + '>').val($this.msgtext).attr('id', _this.editorID).attr('style', 'height: 150px; width: 100%;'),
                        $('<div>').addClass('mac-top-btn').append(
                                $('<button>').addClass('button orange').html('Отправить').attr('data-name', 'send').click('send', _eventSendAnswer),
                                $('<button>').addClass('button grey dark').html('В черновик').attr('data-name', 'indraft').click('draft', _eventSendAnswer),
                                $('<button>').addClass('button grey dark').html('Удалить').attr('data-name', 'del_draft').click('del_draft', _eventSendAnswer),
                                $('<button>').addClass('button grey dark').html('Метки').attr('data-name', 'mark')
                                )
                        ).click(function(e)
                {
                    /*var name = $(e.target).attr('data-name')
                     if (name) config.click(name, e)
                     return false*/
                }),
                $('<td>').addClass('mac-merka-right').append(
                        $('<div>').addClass('mac-work-with-template').append(
                                $('<button>').addClass('button grey dark').html('Создать шаблон').attr('data-name', 'template_create'),
                                $('<div>').addClass('mac-gear-btn')
                                ).click(function(e)
                        {
                            var name = $(e.target).attr('data-name');
                            if (name) {
                                config.click(name, e);
                            }
                        }),
                        $('<div>').addClass('mac-template-list-conteiner').html(
                                _this.blocks.templateList = $('<ul>').addClass('mac-template-list').click(function(e)
                                {
                                    var
                                            target = $(e.target),
                                            name = target.attr('data-name')
                                    if (name) {
                                        config.click(name, e)
                                        target.parent().find('.mac-template-item').removeClass('selected')
                                        target.addClass('selected')
                                        return false
                                    }
                                })
                                )
                        )
                ))

        $.each(config.templateList, function()
        {
            _this.blocks.templateList.append(
                    $('<li>').addClass('mac-template-item').attr('data-name', this.name).html(this.text)
                    )
        })

        _this.append(_layout)
    }

    function _initBMark()
    {
        // Указываем, каким цветом вывести окно ответа, взависимости от типа сообщения
        var u_cfg = $.conf['u_cfg'];
        if($this.conf['state'] == 'draft' && typeof u_cfg['draft'] != 'undefined')
        {
            $this.background_color =  u_cfg['draft']['value'];
        }

        if($this.conf['send_state'] == 'queue' && typeof u_cfg['queue'] != 'undefined')
        {
            $this.background_color =  u_cfg['queue']['value'];
        }

        if($this.conf['send_state'] == 'process' && typeof u_cfg['process'] != 'undefined')
        {
            $this.background_color =  u_cfg['process']['value'];
        }

        if($this.conf['send_state'] == 'error' && typeof u_cfg['error'] != 'undefined')
        {
            $this.background_color =  u_cfg['error']['value'];
        }
        
        var ctrl = $('<div>').addClass('mac-marks').click(function(e)
        {
            // Переключение вкладок взависимости от клика по ней
            var target = $(e.target)

            // Изменение поля reply_layer_id
            $this.reply_layer_id = target.attr('id');

            if (target.hasClass('mac-mark-btn') && $this.new_window == false)
            {
                config.click(target.attr('id'), e)
                target.parent().find('.mac-mark-btn').removeClass('selected').css({backgroundColor: $this.background_color});
                target.removeAttr('style', 'background-color').addClass('selected');
                _changeRequestLayer();

                //return false
            }
        })

        $.each(config.markList, function()
        {
            // Добавление класса selected к слою по умолчанию
            // Если слой из массива равен дефолтному (определяется так же на сервере)
            // добавить класс selected, т.е. сделать вкладку - слой активной (ым)
            if(this['default_layer'] == true)
            {
                ctrl.append(
                    $('<div>').addClass('mac-mark-btn selected').html(this.text).attr('data-name', this.name).attr('id', this.layer_id)
                    )
            }
            else
            {
                ctrl.append(
                    $('<div>').addClass('mac-mark-btn').html(this.text).attr('data-name', this.name)
                            .attr('id', this.layer_id).css({backgroundColor: $this.background_color})
                    )
            }
        })

        _this.blocks.marks = ctrl
        _this.append($('<div>').attr('id', _this.editorID + '_footerID'), ctrl)
    }

    this.scrollTo = function()
    {
        $('html, body').stop().animate({scrollLeft: 0, scrollTop:$('#' + _this.editorID + '_footerID').offset().top}, 1000);
        $('#' + _this.editorID).prev().children().trigger('click')
    }
    //console.dir($this)
    //Объявление дефолтного конфига

    this._config = {
        renderTo: 'body',
        fortext: 'Some text for here',
        msgtext: 'Its cool body of my baby',
        textareaType: 'textarea',
        markList: 
            // Идентификация слоёв для вкладок, определяется сервером
            // параметр определён как array()
            $this.layers
        ,
        templateList:[
            {name: 'tmp1', text: 'template #1'},
            {name: 'tmp2', text: 'template #2'},
            {name: 'tmp3', text: 'template #3'},
            {name: 'tmp4', text: 'template #4'}
        ],
        click: function(name, event)
        {

        },
        initEditor: function(id)
        {
            var e = setTimeout(function()
            {
                $this._editor = $($this.blocks.msgtext).redactor({ focus: true });
                $this._editor.setHtml($this.msgtext);
            }, 50);
        }
    }

    // Слияние дефотного и переданного при создании
    config = $.extend(this._config, config)
    // Объявление методов класса
    $.extend(this, {})

    //Инициализация и создание объекта
    $.extend(this, $('<div>').addClass('msg-answer-ctrl'))
    var _this = this

    // Инициализация и рендеринг вкладок - слоёв
    _initBMark()

    // Инициализация и рендеринг каркаса окна ответа с внесёнными в него уже текстом и поле кому
    _initLayout()

    var session = $.cookie('PHPSESSID');
    $(config.renderTo).append(this)

    console.time("timing init editor");
    // Инициализация редактора в окне ответа
    $this._editor = config.initEditor(_this.editorID);
    // Зарпуск автосохранения
    if(typeof $this._editor == 'object')
    {
        _autoSave();
    }

    this.blocks.marks.find('div:eq(0)').trigger('click')
    console.timeEnd("timing init editor");

    // GIF анимация при загрузке файлов через аплоадер
    var spinner = $("<img class='spinner'  src='/.design/scripts/widgets/AjaxUpload/img/loading.gif'/>");
    // Инициализируем плагин - виджет загрузки файлов
    var UPLOADER = new AjaxUpload('attachID' + _this.editorID,
        {
        // path to server-side upload script
        action: 'index.php',
        // additional data to send, name-value pairs
        data: {
            'session' : session,
            'thread_id' : $this.thread_id,
            'msg_id' : $this.reply_msg_id,
            'dispatch' : 'Message',
            'method' : 'addAttachment'
        },
        // validation
        // ex. ['jpg', 'jpeg', 'png', 'gif'] or []
        allowedExtensions: [],
        // each file size limit in bytes
        // this option isn't supported in all browsers
        sizeLimit: 0, // max size
        minSizeLimit: 0, // min size
        // set to true to output server response to console
        debug: true,
        // Submit file after selection
        autoSubmit: true,
        responseType: 'JSON',
        // events
        // you can return false to abort submit
        onSubmit: function(id, fileName)
        {
            //alert('Submit')
        },
        onChange: function(id, fileName, loaded, total)
        {
            //alert('Change')
            _this.blocks.attachBlockComplete.append(spinner);
        },
        onComplete: function(id, response)
        {
            spinner.remove();
           _setAttachmentInAreaAnswer(response);
        },
        onCancel: function(id, fileName)
        {
            alert('The upload of ' + id + ' has been canceled!');
        },
        messages: {
            serverError: "Some files were not uploaded, please contact support and/or try again.",
            typeError: "{file} has invalid extension. Only {extensions} are allowed.",
            sizeError: "{file} is too large, maximum file size is {sizeLimit}.",
            emptyError: "{file} is empty, please select files again without it."
        },
        showMessage: function(message){ alert(message); }
    })

    function _setAttachmentInAreaAnswer(response)
    {
        if(typeof response.attach_id == 'undefined')
        {
            alert('Attachment not create!!!');
        }
        var check = Object();
        var attach = Object();
        var fileObj = {
            'filePath' : response.filePath,
            'name' : response.name,
            'id' : response.attach_id,
            'size' : response.size
        }
        //console.log(fileObj)
        _this.blocks.attachBlockComplete.append(
                attach = $('<div>').addClass('mac-added-attachs').append(
                        check = $('<input>').attr('type', 'checkbox').attr('checked', 'checked').attr('id', 'ch' + fileObj.id)
                                .attr('value', fileObj.name).addClass('mac-aa-cb'),
                        $('<a>').attr({
                            'href'    : fileObj.filePath,
                            'target': 'blank',
                            'class'    : 'mac-aa-href'
                        }).html(fileObj.name + ' ' + (fileObj.size / 1024).toFixed(2) + 'Кб')
                        )
                )

        var obj = {
            attch : attach,
            attch_id : fileObj.id
        }

        $(check).change(obj, function()
        {
             if ($(this).is(':checked') === false)
             {
                // Вызов обработчика удаления файла
                 var del = CompositeController.RUN_EVENT({
                    Message: {
                        action: 'delete_attachment',
                        cache: true,
                        data: {
                            thread_id : $this.thread_id,
                            msg_id : $this.reply_msg_id,
                            attch_id : parseInt(obj.attch_id, 10)
                        }
                    }
                });

                 // Если удаление произошло успешно
                if(del == true)
                {
                    obj['attch'].remove();
                }
             }
        })
    }

    this.scrollTo();

    return this;
}
