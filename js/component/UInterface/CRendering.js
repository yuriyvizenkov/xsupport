﻿/**
 * Author: Greg
 * Date: 23.03.11
 * Рендеринг результатов действий из контроллеров
 */
CRendering.LIST_OPEN_ANSWERS = Array();

function CRendering(data)
{
    /**
     * Данные, от сервера в формате JSON и контролллера
     * Формат передаваемых данных пример:
     * [ {text: "Request it`s sucessfuly!"} ]
     */
    this.data = data;

    /**
     * Ссылка на самого себя для облегчения вызова своих методов
     */
    this._selfObj = null;

    cuSel({changedEl: ".dropdown select",visRows: 8,scrollArrows: true});
    cuSel({changedEl: ".searchDD select",visRows: 8,scrollArrows: true});

}

/**
 * Интерфейс класса. Запускает определённый метод из пакета данных
 * @param method
 */
CRendering.prototype.run = function(method)
{
    if (typeof method == 'undefined') {
        return  new Error('Undefined method ' + method);
    }

    // Вызов метода определяемого в uiComponent
    this._selfObj[method]();
}

/**
 * Рендеринг макета страницы
 */
CRendering.prototype.main = function()
{
    // После авторизации убираем со страницы форму авторизации
    if ($('#block_auth').length)
    {
        $('#block_auth').remove();
    }

    // Устанавливаем серверное время
    if ($.conf['server_hours'] == false)
    {
        $.conf['server_hours'] = this.data['data']['server_hours'];
    }

    // Записываем пользовательскую конфигурацию
    if ($.conf['u_cfg'] == false && this.data['data']['u_cfg'] != false)
    {
        $.conf['u_cfg'] = this.data['data']['u_cfg'];
    }

    // Вычисляем разность времени
    var date = new Date();
    var local_hours = date.getHours();
    // Запоминаем разность в конфигурации
    $.conf['difference_hours'] = parseInt($.conf['server_hours']) - parseInt(local_hours);

    this._selfObj.setTemplate('main');
    $('#layout').empty();
    $('#main').tmpl(this.data['data']).appendTo('#layout');
    this.setTitle('Главная');
}

/**
 * Форма авторизации
 */
CRendering.prototype.auth = function()
{
    if ($('#block_auth').length) {
        if (this.data['data']['result'] == 'error') {
            this._selfObj.error();
        }
        return false;
    }

    this._selfObj.setTemplate('auth');

    $('#layout').empty();

    $('#auth').tmpl().appendTo('#content');
    this.setTitle('Авторизация');
}

/**
 * Рендеритнг меню на странице
 */
CRendering.prototype.menu = function()
{
    this._selfObj.setTemplate('menu');
    this._selfObj.setTemplate('tags_menu');
    $('.base_menu').empty();
    $('.tags_menu').empty();
    $('#menu').tmpl(this.data['data']).appendTo('.base_menu');
    $('#tags_menu').tmpl(this.data['data']).appendTo('.tags_menu');

    // Запоминаем выбранный пункт меню (фильтр)
    $('#current_selected_filter').val(this.data['data']['selected_filter']);
}

CRendering.prototype.settings = function()
{
    this._selfObj.setTemplate('settings');
    $('#mainConteiner').empty();
    /*$('.conteiner-bottom-block').hide();
    $('.head-block').hide();*/

    if($.conf['u_cfg'] !== false)
    {
        $('#settings').tmpl($.conf).appendTo('#mainConteiner');
        $('input.color').mColorPicker();

        var u_cfg = $.conf['u_cfg'];
        // Связываем события изменения цвета в настройках
        $.each(u_cfg, function(index, value)
        {
            if($('#' + index).length > 0)
            {
                $('#' + index).bind('colorpicked', index, function(event)
                {
                    var value = $(this).val();
                    var name = $(this).attr('id');
                    var main = CompositeController.RUN_EVENT({
                        Main: {
                            action: 'change_settings',
                            cache: true,
                            data: {
                                name : name,
                                value : value
                            }
                        }
                    });
                    $.conf['u_cfg'][name]['value'] = value;
                });
            }
        });
    }
    else
    {
        var empty_array = [];
        $('#settings').tmpl(empty_array).appendTo('#mainConteiner');
    }

    this.setTitle('Настройки');
}

/**
 * Рендеринг кортежа тредов
 */

var _threadG = false
CRendering.prototype.threads = function()
{
    var threads_list = this.data.data.threads_list
    //var g_is_spam_folder = ($("#is_spam_folder").length == 1) ? true : false

    var g_is_spam_folder = (this.data.data.selected_filter == 'spam') ? true : false

    if (g_is_spam_folder)
    {
        $('#mainConteiner').addClass('spam-folder')
    }
    else
    {
        $('#mainConteiner').removeClass('spam-folder')
    }


    var g_cortege_threads = new cortegeThreads({
        renderTo: "#mainConteiner",
        defaultItem: {
            listeners: {
                "dblclick": function()
                {
                },
                "mouseenter": function()
                {
                    /*prevStar = this.mark.tmCss();
                     this.mark.tmCss("yellow");*/
                },
                "mouseleave": function()
                {
                    //this.mark.tmCss(prevStar);
                }
            },
            onClick: function(name, el)
            {
                if (name == "select")
                    return false;
                var import_msg_id = parseInt(this.import_msg_id);
                var thread_id = parseInt(this.thread_id);
                if (typeof import_msg_id == 'undefined')
                    return false;

                if (typeof thread_id == 'undefined')
                    return false;
                if (g_is_spam_folder) {
                    var data = {
                        message_id : import_msg_id,
                        spam : g_is_spam_folder
                    }
                    var action = 'getListSpam';
                }
                else {
                    var data = {
                        thread_id : thread_id,
                        spam : g_is_spam_folder
                    }
                    var action = 'getListMessages';
                }

                var messages = CompositeController.RUN_EVENT_HISTORY({
                    Message: {
                        action: action,
                        data : data
                    }
                });
            }
        },
        afterInit: function ()
        {
            //$(".c-rounded-body.cortege_threads").show();
        }
    });
    _threadG = g_cortege_threads

    if (g_is_spam_folder) {
        var i = 0
        $.each(threads_list, function()
        {
            if (this.import_box == 'span') this.isanswer = true
            if (this.import_box == 'span?') this.isanswer = false
            this.noassign = false;
            this.tags = [
                {name:this.import_box}
            ]
        })
    }

    // Заливаем аякс в объект отрисовки кортежа
    g_cortege_threads.ctProxy(threads_list)

    $('.control-block-left-side').html('')
    $('.control-block-right-side').html('')

    if (!g_is_spam_folder) {
        var cbctrlCfg = {
            actionList:[
                {value:'act1', name:'Действие #1'},
                {value:'act2', name:'Действие #2'},
                {value:'act3', name:'Действие #3'},
                {value:'act4', name:'Действие #4'}
            ],
            menuList: [
                {name: 'Все', type: 'all'},
                {name: 'Ни одного', type: 'nothing'},
                {name: 'Отвеченные', type: 'isanswer'},
                {name: 'Все кроме отвеченных', type: 'notanswer'},
                {name: 'Просмотренные', type: 'isview'},
                {name: 'Не просмотренные', type: 'notview'},
                {name: 'Все "No assign"', type: 'noassign'},
                {name: 'Все кроме "No assign"', type: 'notnoassign'}
            ],
            delegation: function(n, t)
            {
                var eqF
                switch (t) {
                    case 'all':
                        eqF = function()
                        {
                            return true
                        };
                        break;
                    case 'nothing':
                        eqF = function()
                        {
                            return false
                        };
                        break;
                    case 'isanswer':
                        eqF = function(o)
                        {
                            return o.cteIsanswer()
                        };
                        break;
                    case 'notanswer':
                        eqF = function(o)
                        {
                            return !o.cteIsanswer()
                        };
                        break;
                    case 'isview':
                        eqF = function(o)
                        {
                            return o.cteIsview()
                        };
                        break;
                    case 'notview':
                        eqF = function(o)
                        {
                            return !o.cteIsview()
                        };
                        break;
                    case 'noassign':
                        eqF = function(o)
                        {
                            return o.cteNoassign()
                        };
                        break;
                    case 'notnoassign':
                        eqF = function(o)
                        {
                            return !o.cteNoassign()
                        };
                        break;
                }
                $.each(g_cortege_threads.ctItems, function()
                {
                    this.cteSelect(eqF(this))
                })

            },
            check: function(flag)
            {
                $.each(g_cortege_threads.ctItems, function()
                {
                    this.cteSelect(flag)
                })
            }
        }
    } else {
        var cbctrlCfg = {
            menuList: [
                {name: 'Все', type: 'all'},
                {name: 'Ни одного', type: 'nothing'},
                {name: 'spam', type: 'spam'},
                {name: 'spam?', type: 'spam?'}
            ],
            delegation: function(n, t)
            {
                var eqF
                switch (t) {
                    case 'all':
                        eqF = function()
                        {
                            return true
                        };
                        break;
                    case 'nothing':
                        eqF = function()
                        {
                            return false
                        };
                        break;
                    case 'spam':
                        eqF = function(o)
                        {
                            return (o._config.import_box == 'spam')
                        };
                        break;
                    case 'spam?':
                        eqF = function(o)
                        {
                            return (o._config.import_box == 'spam?')
                        };
                        break;
                }
                $.each(g_cortege_threads.ctItems, function()
                {
                    this.cteSelect(eqF(this))
                })
            },
            check: function(flag)
            {
                $.each(g_cortege_threads.ctItems, function()
                {
                    this.cteSelect(flag)
                })
            }
        }
    }


    new gCBCtrl($.extend(cbctrlCfg, {
        renderTo: '.control-block-left-side:eq(0)'
    }))
    new gCBCtrl($.extend(cbctrlCfg, {
        renderTo: '.control-block-left-side:eq(1)',
        showMenuCSS: 'bottomCtrl'
    }))

    g_is_spam_folder ? $('.control-block-left-side').append(
            $('<button class="button grey">Не спам</button>'),
            $('<button class="button grey">Спам</button>')
    ) : ''

    // Удаление блока оповещения закрытия треда
    if ($('#div_thread').length) {
        $('#div_thread').remove();
    }
    // Добавление пейджера на страницу кортежа тредов
    var gmlPg = new gmLikePagging({
        renderTo: '.control-block-right-side',
        currentPage: ~~this.data['data']['current_page'],
        recOnPage: $.conf['PAGINATION_THREADS_PER_PAGE'],
        recsCnt: this.data['data']['count_threads'],
        filter: this.data['data']['selected_filter']
    })
    $('#main-conteiner-loder-indicator-id').hide();

    // Если текущая страница не содержит спам сообщения показываем панели управления
    if(this.data['data']['current_view'] != 'spam' || typeof this.data['data']['current_view'] == 'undefined')
    {
        $('.control-block-left-side').show();
    }
}

var compilgmLikePagging = false;
/**
 * Рендеринг кортежа сообщений
 */
var _msgG = false
CRendering.prototype.message = function()
{
    var messages_list = this.data.data.messages_list;

     if(this.data['data']['current_view'] != 'spam')
    {
        $('td.answer').show();
        $('.control-block-left-side').show();
        $('.cme-bottom-line').show();
    }

    /*if ($(".c-rounded-body.cortege_messages").length) {*/
    var g_cortege_message = new cortegeMessages({
        renderTo: "#mainConteiner",
        afterInit: function ()
        {
            //$(".c-rounded-body.cortege_messages").show();
        },
        menuList : function (msg_config)
        {
            return detectMenuForMessage(msg_config);
        },
        defaultItem:{
            initAnswerBtn: function (data, _this)
            {
                // Верхнее меню в сообщении
                var glike =  new glAnswerBtnCtrl({
                    renderTo: false,
                    menuList: detectMenuForMessage(_this._config),
                    /**
                     * Инициация событий взависимости от клика верхнего подменю.
                     * Определяется по типу подменю и вызывает событие определённого характера
                     * @param n текст подменю
                     * @param t тип подменю || идентификатор подменю
                     */
                    delegition: function(n, t)
                    {
                        if (t == 'answer' || t == 'edit') clickAnswer(_this._config, _this._bodyList.editorPlace);
                        if (t == 'in_not_processed' || t == 'in_processed') inProcessedMessages(_this);
                    },
                    /**
                     * Функция инициирует вызов окна ответа, и вызывается по клику "Ответить" или "Редактировать"
                     * Определяется для верхнего меню.
                     */
                    answerClick: function()
                    {
                        if(typeof _this._bodyList.editorPlace != 'undefined')
                        {
                            clickAnswer(_this._config, _this._bodyList.editorPlace);
                            // Если окно инициализировалось, отключаем клики по меню
                            if(typeof _this._bodyList.glikeAnswer != 'undefined')
                            {
                                _this._bodyList.glikeAnswer.hideMenu();
                                delete(_this._bodyList.editorPlace);
                            }
                        }
                    },
                    data : data
                })

                return glike;
            },
            /**
             * Функция инициирует вызов окна ответа, и вызывается по клику "Ответить" или "Редактировать".
             * Определяется для нижнего меню.
             * @paюлram _this объект слайдер-блок сообщения
             */
            answerClick: function(_this)
            {
                clickAnswer(_this._config, _this._bodyList.editorPlace);
                // Если окно инициализировалось, отключаем клики по меню
                if(typeof _this._bodyList.glikeAnswer != 'undefined')
                {
                    _this._bodyList.glikeAnswer.hideMenu();
                    delete(_this._bodyList.editorPlace);
                }
            },
            /**
             * Клик по контролу "В обработанные"
             * @param t объект слайдер-блок сообщения
             */
            inTreatedClick: function(t)
            {
                inProcessedMessages(t);
            }
        }

    });

    // Функция обработчик определяет пункты меню для вывода
    function detectMenuForMessage(data)
    {
        
         // Если сообщение ещё не считается отосланным (физически) выводим управляющие элементы
        if (data['send_state'] != 'process' && data['send_state'] != 'error') {

            // Вывод контролов взависимости от контекста сообщения к пользователю
            // Если сообщение имеет контекст редактирования (edit), определяется сервером
            if (data['editType'] == 'edit')
            {
                if (data['send_state'] != 'send') {
                    var answer_menu = {name: 'Редактировать', type : 'edit', id : 'edit_answer'};
                }
            }
            else
            {
                var answer_menu = {name: 'Ответить', type: 'answer', id : 'answer_send'};
            }

            // Если сообщение считается "собственного слоя" не выводим нижеследующие контролы
            if (data['layer_write'] == true)
            {
                // Если сообщение обработано выводим что оно обработано и является противоположным контролом
                // который изменяет сообщение на не обработанное
                if (data['state_processing'] == 'answered') {
                    var processed_menu = {name: 'В необработанные', type: 'in_not_processed', id: 'in_not_processed'}
                }
                // Иначе выводим контрол
                else
                {
                    var processed_menu = {name: 'В обработанные', type: 'in_processed', id: 'in_processed'};
                }
            }

            var menu = {
                answer : answer_menu,
                processed : processed_menu
            };
        }
        else
        {
            var menu = [];
        }
        return menu;
    }

    /**
     * Инициализация назначения "в обработанные" всех сообщений  по иерархии выше, от которого произведён клик
     * @param t
     */
    function inProcessedMessages(t)
    {
        var type = t._bodyList.blInTreated.attr('id');

        // Воспроизведение события "В обработанне" и "Не обработанные"
        var prcs = CompositeController.RUN_EVENT({
            Message: {
                action: 'inProcessedMessage',
                cache: true,
                data: {
                    g_cortege_message : g_cortege_message,
                    type_processed : type,
                    msg_id : t._config['msg_id'],
                    thread_id : t._config['thread_id']
                }
            }
        });
    }

    /**
     * Метод для обработки контрола "Ответить" или "Редактировать"
     * В контексте "Ответить" создаёт пустое сообщение - ответ для сообщения, msg_id которого отсылается на сервер и
     * затем проверяется алгоритмом на сервере разрешено ли оно для ответа. Возврщаемые данные вставляются в окно ответа.
     * В контексте "Редактировать" только отсылается msg_id сообщения, для которого требуется ответ, и проверяется
     * алгоритмом для разрешения отвечать на него.
     * @param _config
     * @param ctrlAnswer
     */
    function clickAnswer(_config, renderTo)
    {
        console.time("timing full request answer") ;
        /**
         * Данные для сервера:
         * 1. msg_id - идентификатор сообщения требуемого для ответа
         * 2. thread_id - идентификатор треда, к которому принадлежит данное сообщение
         * 3. edit - контекст клика контрола (Редактировать (edit) или Ответить(send))
         * 4. reply_layer_id - слой для ответа
         */
        var answer = CompositeController.RUN_EVENT({
            Answer: {
                action: 'answer',
                cache: true,
                data: {
                    // Область в которую рендерится окно ответа
                    r : renderTo,
                    msg_id : _config['msg_id'],
                    thread_id : _config['thread_id'],
                    reply_layer_id : _config['reply_layer_id'],
                    edit : _config['editType']
                }
            }
            // TODO: ДОБАВИТЬ ОБНОВЛЕНИЕ МЕНЮ ЗДЕСЬ И УБРАТЬ В КОНТРОЛЛЕРЕ РЕНДЕРИНГ МЕНЮ В МЕТОДЕ "ANSWER"
        });

        // Если контекст "Ответить" все более "старшие" сообщения этого треда помечаются как обработанные
        if (_config['editType'] == 'send') {
            var prcs = CompositeController.RUN_EVENT({
                Message: {
                    action: 'inProcessedMessage',
                    cache: true,
                    data: {
                        g_cortege_message : g_cortege_message,
                        type_processed : 'in_processed',
                        msg_id : _config['msg_id'],
                        thread_id : _config['thread_id']
                    }
                }
            });
        }
    }

    _msgG = g_cortege_message

    g_cortege_message.cmProxy(messages_list)

    $('.control-block-left-side').html('')
    $('.control-block-right-side').html('')

    // Если тред закрыт вывести уведомление
    if (this.data['data']['thread_close'] == 'closed') {
        if (!$('#div_thread').length) {
            $('.content').append(
                    $('<div class="cover-bootom-header gradient" id="div_thread"></div>').html('<h2 align="center">Тред закрыт</h2>')
            )
        }
        // Инициализация кнопки взависимости от состояния треда
        var thread_action = 'Открыть нить';
        var thread_id = 'open_thread';
    }
    else {
        // Инициализация кнопки взависимости от состояния треда
        var thread_action = 'Закрыть нить';
        var thread_id = 'close_thread';

        // Если был клик по открытию треда, удаляем строку - сообщение "Тред закрыт"
        if ($('#div_thread').length) {
            $('#div_thread').remove();
        }
    }

    $('.control-block-left-side').append(
            $('<button class="button grey" id=' + thread_id + '></button>').html(thread_action),
            $('<button class="button grey" id="tags"></button>').html('Метки'),
            $('<button class="button orange" id="add_message"></button>').html('Добавить сообщение'),
            $('<input name="thread_id" type="hidden">').val(this.data['data']['current_thread']['thread_id'])
    )

    this.setTitle('Сообщения из треда: ' + this.data['data']['current_thread']['thread_id']);
    $('#main-conteiner-loder-indicator-id').hide();

    // Если текущая страница содержит спам сообщение убираем панель управления
    if(this.data['data']['current_view'] == 'spam')
    {
        $('.control-block-left-side').hide();
    }
    // Иначе показываем её
    else
    {
        $('.control-block-left-side').show();
    }

    // перематываем вверх
    $('body,html').animate({scrollTop:0},50);
}

/**
 * Метод изменяет контролы "В обработанные" в обратный контекст и изменяет стили оформления слаёдеров тех сообщений,
 *
 */
CRendering.prototype.inProcessed = function()
{
    // Получение идентификаторов сообщений, которые были установлены как обработанные
    var list_processed_msg = this.data['data']['list_processed_msg'];
    // Если было событие "В необработанные" возращается одно сообщение,
    // для которого изменяется состояние
    var processed_msg = this.data['data']['processed_msg'];
    var g_cortege_message = this.data['data']['g_cortege_message'];

    // Если возвращённое значение не является неопределённым, то есть сообщения обработаны
    if (typeof list_processed_msg != 'undefined')
    {
        var i_list = list_processed_msg.length - 1;
        var j_cortege = g_cortege_message.cmItems.length - 1;
        // Проходим по возращённому массиву
        for (var i = 0; i <= i_list; i++)
        {
            // Проходим по сообщениям из треда и помечаем определённые сервером как обработанные
            for (var j = 0; j <= j_cortege; j++)
            {
                // Если сообщение найдено (идентификаторы равны)
                if (parseInt(list_processed_msg[i].msg_id) == parseInt(g_cortege_message.cmItems[j]._config.msg_id))
                {
                    // Обработка сообщений на предмет изменения надписи "сообщение обработанно",
                    // изменение стиля дайджеста и добавление идентификатора того что сообщение обаработано
                    if (typeof g_cortege_message.cmItems[j]._bodyList.blInTreated != 'undefined')
                    {
                        var blControl = g_cortege_message.cmItems[j]._bodyList.blInTreated;
                        blControl.empty().html('<span></span>В не обработанные');
                        blControl.attr('id', 'in_not_processed');
                        // Изменение иконки контрола нижнего меню
                        if(blControl.hasClass('cmebl-btn-in_processed'))
                        {
                            blControl.removeClass('cmebl-btn-in_processed');
                        }
                        blControl.addClass('cmebl-btn-in_not_processed');

                        // Изменяем стиль строки дайджеста
                        g_cortege_message.cmItems[j]._bodyList.digest.removeAttr('style');
                        g_cortege_message.cmItems[j]._bodyList.tags_digest.removeAttr('style');

                        // Изменяем верхнее в углу меню у сообщения
                        g_cortege_message.cmItems[j]._bodyList.glikeAnswer.
                                _domList.menuItems.processed.empty().html('В не обработанные');
                        g_cortege_message.cmItems[j]._bodyList.glikeAnswer.
                                _domList.menuItems.processed.attr('id', 'in_not_processed');

                        break;
                    }
                }
            }
        }
    }

    if(typeof processed_msg != 'undefined')
    {
        var i_cortege = g_cortege_message.cmItems.length - 1;
        // Находим сообщение, которое инициировало событие изменения состояния
        for (var i = 0; i <= i_cortege; i++)
        {
            // Если сообщение найдено (идентификаторы равны)
            if (parseInt(processed_msg.msg_id) == parseInt(g_cortege_message.cmItems[i]._config.msg_id))
            {

                // Обработка сообщений на предмет изменения надписи "сообщение обработанно",
                // изменение стиля дайджеста и добавление идентификатора того что сообщение обаработано
                if (typeof g_cortege_message.cmItems[i]._bodyList.blInTreated != 'undefined')
                {
                    // Контрол нижнего меню
                    var blCtrl = g_cortege_message.cmItems[i]._bodyList.blInTreated;
                    blCtrl.empty().html('<span></span>В обработанные');
                    blCtrl.attr('id', 'in_processed');
                    // Изменение иконки контрола нижнего меню
                    if(blCtrl.hasClass('cmebl-btn-in_not_processed'))
                    {
                        blCtrl.removeClass('cmebl-btn-in_not_processed');
                    }
                    blCtrl.addClass('cmebl-btn-in_processed');

                    // Изменяем верхнее в углу меню у сообщения
                    g_cortege_message.cmItems[i]._bodyList.glikeAnswer.
                            _domList.menuItems.processed.empty().html('В обработанные');
                    g_cortege_message.cmItems[i]._bodyList.glikeAnswer.
                            _domList.menuItems.processed.attr('id', 'in_processed');

                    // Изменяем стиль строки дайджеста
                    g_cortege_message.cmItems[i]._bodyList.digest.attr('style', 'font-weight:bold');
                    g_cortege_message.cmItems[i]._bodyList.tags_digest.attr('style', 'background-color:#F5F5F5');
                    break;
                }
            }
        }
    }
}

/**
 * Метод производит действия над окном ответа в момент переключения вкладок (изменение слоёв)
 */
CRendering.prototype.changeLayer = function()
{
    var ctrlAnswer = this.data['data']['ctrlAnswer'];
    var msg = this.data['data'];
    ctrlAnswer['reply_to'] = msg['reply_to'];
    $('#to_' + ctrlAnswer['editorID']).empty().val(msg['reply_to']);
}

/**
 * Рендеринг области ответа для определённого сообщения возвращённого сервером
 */
CRendering.prototype.answer = function()
{
    console.time("timing render method answer");
    /**
     * Получаемые данные от сервера:
     * Если сообщение найдено для ответа: (object) msg - объект сообщение со своими данными
     * Если сообщение не найдено: (bool) msg - false и окно ответа не выводится так как сообщение не существует
     * или не разрешено
     * renderTo - Область, в которую рендерится окно ответа (не используется при сценариях "Создать сообщение" и
     * "Добавить сообщение")
     * layers - массив из слоёв доступных для пользователя
     * edit - контекст сообщения
     */
    var msg = this.data['data']['msg'];
    msg['layers'] = this.data['data']['layers'];

    // Если сообщение не найдено или не разрешено к ответу не выводим окно ответа
    // Если прораммист не сделал вывод ошибки с сервера
    if (msg == false)
    {
        // Вывод окна сообщения что сообщение для данного слоя не найдено или не разрешено для ответа
        alert('Данное сообщение является не допустимым для ответа')
        return false;
    }

    // Инициализируем окно ответа с редактором (объект управления окном ответа)
    var ctrlAnswer = new answerMsgCtrl({'renderTo' : this.data['data']['renderTo']}, msg);
    // Добавляем открытое окно ответа в список
    CRendering.LIST_OPEN_ANSWERS.push(ctrlAnswer);

    // Инициализируем первоначальное окно ответа
    ctrlAnswer['reply_to'] = msg['to'];
    ctrlAnswer['reply_layer_id'] = msg['layer_id'];
    ctrlAnswer['edit'] = this.data['data']['edit'];

    // Указываем флаг того что окно уже загружено (используется для того что бы вкладки не сбрасывались)
    // то есть в answerctrl.js (стр. 286 (_initBMark())) написан алгоритм переключения вкладок - слоёв
    ctrlAnswer['new_window'] = false;

    // Определение окна ответа
    if (typeof this.data['data']['type_answer'] != 'undefined') {
        ctrlAnswer['type_answer'] = this.data['data']['type_answer'];
    }

    // Если тип окна ответа предназначен для создания нового сообщения в определённом треде,
    // добавляем в "среднюю часть" страницы
    if (ctrlAnswer['type_answer'] == 'new_msg') {
        $('.main-side').append($('<div>').addClass('new-msg').append(ctrlAnswer));
    }

    // Если тип окна ответа предназначен для создания нового сообщения и нового треда
    if (ctrlAnswer['type_answer'] == 'new_thread') {
        $('#mainConteiner').empty();
        ctrlAnswer['thread_id'] = msg['thread_id'];

        var g_cortege_messages = new cortegeMessages({renderTo: "#mainConteiner"});
        $(g_cortege_messages).append($('<div>').addClass('new-msg').append(ctrlAnswer));
    }

    console.timeEnd("timing render method answer");
    console.timeEnd("timing full request answer")
}

CRendering.prototype.change_support = function()
{
    var period = this.data['data']['period'];
    $('#schedule').empty('');
    $('#schedule').html('График: <b>' + period['date_start'] + ' - ' + period['date_end'] + '</b>');
}

/**
 * Загрузка и установка шаблона
 * @param string
 */
CRendering.prototype.setTemplate = function(template)
{
    // Если шаблон для использования уже загружен или находится в "кэше"
    if ($('script#' + template).length > 0) {
        return true;
    }

    $.ajax({
        type: 'POST',
        url: '/.templates/' + template + '.jstpl',
        async: false,
        dataType: 'html',
        error: function()
        {
            alert('Template' + template + ' not load!');
            return false;
        },
        success: function(data)
        {
            $('.script').append(data);
        }
    });

    return true;
}

CRendering.prototype.error = function()
{
    $('#error').html('<span style="color:red;"><b>' + this.data['data']['msg'] + '</b></span>');
}

/**
 * Определение title на странице
 * @param title
 */
CRendering.prototype.setTitle = function(title)
{
    document.title = title;
}