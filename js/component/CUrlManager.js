/**
 * Author: Greg
 * Date: 13.04.11
 * Компонент UrlManager перехватывает ввод из URL или командной строки в виде url.
 * Преобразует в формат ввода и запуска фреймворка.
 */

function CUrlManager(url)
{
    /**
     * Удаление хоста (первой части до якоря) и разбивка по '/' в масcив
     * @type array
     */
    this._URL = url.replace(/^.*#/, '').split('-');

    /**
     * Первый параметр URL
     * Запускаемый контроллер
     * @type string
     */
    this.controller = this._URL[0];

    /**
     * Второй параметр в URL
     * Запрашиваемое действие в контроллере
     * @type string
     */
    this.action = this._URL[1];

    /**
     * Параметры для запроса
     * @type object
     */
    this.data = new Object();

    /**
     * Разбор строки данных из url и формирование объекта данных для входа в фреймворк
     * @param string
     */
    this._parseData = function(data)
    {
        if(typeof data == 'undefined'){
            return false;
        }
        
        // Получение массива из строки данных URL
        var tempData = data.split('.');
        for(i=0; i < tempData.length; i++)
        {
            var concreteParam = tempData[i].split('~');
            this.data[concreteParam[0]] = concreteParam[1];
        }
    }

    /**
     * Метод возвращает готовый пакет - событие на вход фреймворку
     * @return object
     */
    this.getData = function()
    {
        this._parseData(this._URL[2]);
        var obj = new Object();

        obj[this.controller] = {
            action : this.action,
            data : this.data
        }

        return obj;
    }
}

/**
 * Метод генерирует url строку из "управляющего" объекта например:
 * Thread: {
        action: 'getThreadsView',
        cache: true,
        data: data
    }
 * @param obj
 */
CUrlManager.GET_URL_FROM_OBJECT = function(obj)
{
    var url = '';

    for(var key in obj)
    {
        url += key + '-';

        if(typeof obj[key]['action'] == 'undefined'){
            return false;
        }
        url += obj[key]['action'] + '-';

        if(typeof obj[key]['data'] != 'undefined'){
            for(var keyData in obj[key]['data'])
            {
                if(keyData == '')
                {
                    continue;
                }
                url += keyData + '~' + obj[key]['data'][keyData] + '.';
            }
        }
    }

    return url;
}