/**
 * Author: Greg
 * Date: 23.03.11
 * To change this template use File | Settings | File Templates.
 */

/**
 * Статическое свойство компонента.
 * Очередь для обработки. Для любого экземпляра класса Queue будет одна и та же.
 * Работает в режиме первый вошёл первый вышел (FIFO). Последний элемент добавляется в конец очереди для обработки.
 * Если входящее событие имеет флаг сброса: очередь сбрасывается в виде добавления событию свойства  сброса - "dumped"
 * и это событие добавляется первым.
 *
 * @array
 */
CAjaxManager.QUEUE = new Array();

/**
 * Флаг уапрвления очередью
 */
CAjaxManager.BLOCKED_QUEUE = false;

/**
 * Конструктор компонента AjaxManager
 * @param idEvent
 */
function CAjaxManager(idEvent)
{

    /**
     * Идентификатор события
     */
    this._idEvent = idEvent;
}

/**
 * Статический метод для добавления в очередь события на обработку
 * Структура принимаемого события event:
{
    idEvent: 'id_166494661'
    status: 'high',
    resets: true,
    request: {
        url: 'dispatcher.php',
        data: {
            "id_thread" : "256",
            "support"   : "support"
        }
    }
};
 * @param event
 */
CAjaxManager.prototype.addQueue = function(event)
{
    // TODO: Добавить логику ожидания разблокировки очереди другим процессом
    // Если добавляемое событие имеет флаг сброса - сброс событий в очереди.
    // Применяется не удаление события из очереди, а флаг сброса, этим избегаем зависания процесса
    if (event.resets == true) {
        for (i = 0; i < CAjaxManager.QUEUE.length; i++) {
            CAjaxManager.QUEUE[i]['dumped'] = true;
        }
    }
    CAjaxManager.QUEUE.push(event);
    return true;
}

/**
 * Обработчик очереди и запросов
 */
CAjaxManager.prototype.runAjaxManager = function()
{
    var stop = false;

    // Ожидание "своего" события с помощью цикла
    // Обрыв цикла после десяти проверок
    var i = 0;
    while (!stop) {
        // Идентификация "своего" события по idEvent. Если очередь блокирована или событие не в начале очереди продолжаем цикл
        if (CAjaxManager.BLOCKED_QUEUE == true || CAjaxManager.QUEUE[0]['idEvent'] != this._idEvent) {
            // TODO: Возможно стоит добавить задержку в микросекундах setTimeout
            if(i != 10)
            {
                i++;
                continue;
            }
        }

        // Если событие было сброшено процесс завершается неудачей
        if (CAjaxManager.QUEUE[0]['dumped'] == true) {
            // Удаляем событие из очереди
            CAjaxManager.QUEUE.shift();
            var result = {
                idEvent: this._idEvent,
                error: new Error('Event dumped!')
            }
            return result;
        }

        // Процесс принимает управление и захватом очереди
        CAjaxManager.BLOCKED_QUEUE = true;
        var request = CAjaxManager.QUEUE[0].request;
        var dataForRendering = new Array();
        var url = (request['url']) ? request['url'] : '/index.php';
        $('.animation').attr('style', 'display:block');
        $.ajax({
            url:  url,
            data: request['data'],
            dataType: 'json',
            async: false,
            error: function()
            {
                dataForRendering = new Error('Bad request for server URL: ' + request['url']);
                CAjaxManager.QUEUE.shift();
                CAjaxManager.BLOCKED_QUEUE = false;

                return dataForRendering;
            },
            success: function (data, textStatus)
            {
                dataForRendering = data;
            }
        });

        stop = true;
    }

    $('.animation').attr('style', 'display:none');
    // Удаляем событие из очереди
    CAjaxManager.QUEUE.shift();
    // Освобождаем очередь для других процессов
    CAjaxManager.BLOCKED_QUEUE = false;

    return dataForRendering;
}