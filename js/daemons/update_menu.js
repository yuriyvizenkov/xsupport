/**
 * Author: Greg
 * Date: 01.07.11
 * Daemons обновляет меню по заданному, в конфигурации, интервалу
 */

// Время обновления 5 минут
var time_update = 300000;

var update = setInterval(function()
{
    var menu = CompositeController.RUN_EVENT({
        Main: {
            action: 'setMainMenu',
            data: {
                filter : $('#current_selected_filter').val()
            }
        }
    });
}, time_update);
