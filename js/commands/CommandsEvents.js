/**
 * User: Greg
 * Date: 23.03.11
 * Функции вызовов событий от узлов
 */

$(document).ready(function()
{
    // События компонента "Меню" 
    $('.menu').live("click", function(event)
    {
        // Если событие произошло в шаблоне tags_menu
        if ($(this).attr('name') != '') {
            var id = $(this).attr('id');
            id = id.split('_');
            var nameMenu = $(this).attr('name').replace('?', '');

            var data = {
                filter : 'tag',
                tag_id : id[1],
                start : 0,
                count : $.conf['PAGINATION_THREADS_PER_PAGE'],
                // Передача параметра в действие для определения "выбранного меню"
                currentMenu : nameMenu
            }
        }
        else
        {
            // Если событие произошло в шаблоне menu
            var filter = $(this).attr('id');

            if(filter == '')
            {
                alert('Filter for menu empty');
                return false;
            }
            var data = {
                filter : filter,
                start : 0,
                count : $.conf['PAGINATION_THREADS_PER_PAGE']
            }
        }

        var menu = CompositeController.RUN_EVENT_HISTORY({
            Thread: {
                action: 'getThreadsForMenu',
                cache: true,
                data: data
            }
        });
    });

    /**
     * Событие обрабатывает реакцию на клик "Закрыть тред"
     */
    $('#close_thread').live("click", function(event)
    {
        if (!confirm("Вы уверены, что хотите закрыть тред?")) {
            return false;
        }

        var thread_id = $('input[name=thread_id]').val();

        var close = CompositeController.RUN_EVENT({
            Message: {
                action: 'closeThread',
                cache: true,
                data: {
                    thread_id : thread_id
                }
            }
        });
    });

    /**
     * Событие обрабатывает реакцию на клик "Открыть тред"
     */
    $('#open_thread').live("click", function(event)
    {
        if (!confirm("Вы уверены, что хотите открыть тред?")) {
            return false;
        }
        var thread_id = $('input[name=thread_id]').val();

        var open = CompositeController.RUN_EVENT({
            Message: {
                action: 'openThread',
                cache: true,
                data: {
                    thread_id : thread_id
                }
            }
        });
    });

    $('#search_button').live("click", function(event)
    {
        var search_text = $('#search_text').val();
        var type_search = $('#searchIDSel').val();

        var search = CompositeController.RUN_EVENT_HISTORY({
            Thread: {
                action: 'getThreadsView',
                cache: true,
                data: {
                    type_search : type_search,
                    text : search_text,
                    filter : 'search',
                    start : 0,
                    count : $.conf['PAGINATION_THREADS_PER_PAGE']
                }
            }
        });

        $('#type_search_for_pagging').val(type_search);
        $('#text_search_for_pagging').val(search_text);
    });

    /**
     * Событие "Создать сообщение"
     */
    $('#create_message').live("click", function(event)
    {
        var answer = CompositeController.RUN_EVENT({
            Answer: {
                action: 'answer',
                cache: true,
                data: {
                    edit : 'send'
                }
            }
        });

        // перематываем вверх
        $('body,html').animate({scrollTop:0},5);
    });

    /**
     * Инициализация и обработка события контрола "добавить сообщение"
     */
    $('#add_message').live("click", function(event)
    {
        var thread_id = $('input[name=thread_id]').val();

        var answer = CompositeController.RUN_EVENT({
            Answer: {
                action: 'answer',
                cache: true,
                data: {
                    edit : 'send',
                    thread_id : thread_id
                }
            }
        });

        // перематываем вверх
        $('body,html').animate({scrollTop:0},5);
    });

    $('#btn_options').live('click', function()
    {
        var main = CompositeController.RUN_EVENT_HISTORY({
            Main: {
                action: 'settings',
                cache: true,
                data: {
                }
            }
        });
    });

    $('#btn_exit').live('click', function()
    {
        $.cookie('support', null);
        $.conf['support'] = '';
        $.cookie('PHPSESSID', null);
        $.conf['historyInit'] = false;

        $('#content').remove();
        window.location.reload();
    });

    /**
     * Функция меняет суппортера для работы за него
     */
    $('#supporters').live("change", function(event)
    {
        var id = parseInt($('#supporters option:selected').val());

        if (!confirm("Вы уверены, что хотите работать за этого суппортера?")) {
            $('#supporters option:selected').attr('selected', 'selected');
            return false;
        }

        var main = CompositeController.RUN_EVENT({
            Main: {
                action: 'change_support',
                cache: true,
                data: {
                    'id_supporter' : id
                }
            }
        });

        var thread = CompositeController.RUN_EVENT({
            Thread: {
                action: 'getThreadsView',
                cache: true,
                data: {
                    selected_filter : 'myinbox',
                    start : 0,
                    count : $.conf['PAGINATION_THREADS_PER_PAGE']
                }
            }
        });
    });

    // Привязываем обработчик клика к полю поиска (#search_text)
    $('#search_text').live('click', function()
    {
        $(this).val('');
    });

    $('#change_project').live('change', function()
    {
        var project_id = $("#change_project").val();
        if (typeof project_id != "undefined")
        {
            var main = CompositeController.RUN_EVENT({
                Main: {
                    action: 'change_project',
                    cache: true,
                    data: {
                        'project_id' : parseInt(project_id)
                    }
                }
            });

            var menu = CompositeController.RUN_EVENT({
                Main: {
                    action: 'setMainMenu',
                    // По умолчанию может быть опущен и равен true
                    cache: false,
                    data: {
                        selected_filter : 'myinbox'
                    }
                }
            });

            var thread = CompositeController.RUN_EVENT({
                Thread: {
                    action: 'getThreadsView',
                    cache: true,
                    data: {
                        selected_filter : 'myinbox',
                        start : 0,
                        count : $.conf['PAGINATION_THREADS_PER_PAGE']
                    }
                }
            });
        }
        else
        {
            alert('Undefined select project ID');
        }
    });
});