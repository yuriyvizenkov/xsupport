/**
 * Author: Greg
 * Date: 23.03.11
 * To change this template use File | Settings | File Templates.
 */

function CThread (data)
{
    // Наследование свойств родительского класса Controller
    Controller.call(this, data);
    this.data['dispatch'] = 'Thread';
}
// Наследование методов родительского класса
CThread.prototype = inherit(Controller);

/**
 * Действие-метод производимое по умолчанию
 */
CThread.prototype.Action = function()
{
    // TODO: Действие производимое по умолчанию, если нужно с сзапросом ajax (get, post, ajax)
    // Пример использования рендеринга. Функция должна будет использоваться как callback
    this.selfObj.render({
        data: dataRendering,
        cache: this.Request.cache
    });
}

CThread.prototype.getThreadsView = function()
{
    $('#main-conteiner-loder-indicator-id').show()

    this.data['method'] = 'getListThreads';
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if(modelData instanceof Error){
        return false;
    }

    this._selfObj.render('threads', {
        cache: this.cache,
        data: modelData
    });

    // Обновление статистики меню
    this._selfObj.render('menu', {
        cache: this.cache,
        data: modelData
    });

    if(typeof this.data["filter"] == 'undefined')
        this.data["filter"] = 'myinbox';

    return true;
}

/**
 * Метод - действие контроллера реагирующий на события клики UI Menu
 */
CThread.prototype.getThreadsForMenu = function()
{
    // Фукнциональные данные
    // TODO: прорабоать перенос обработки в рендеринг
    // Если клик был по меню тегов
    if(typeof this.data["currentMenu"] != 'undefined'){
        // Замена вопросительного знака в имени, так как селектор не может содержать спецсимволов
        var filter = this.data["currentMenu"];
        delete(this.data["currentMenu"]);
    }
    else {
        var filter = this.data["filter"];
    }

    // Специализированные данные для запроса
    this.data['start'] = 0;
    this.data['count'] = $.conf['PAGINATION_THREADS_PER_PAGE'];

     this._selfObj.getThreadsView();

    // TODO: проработать возможность переноса в класс представления Crendering со своим рендерингом
    // Изменения "фокуса" в меню
    $('.left-menu-block').find('li').attr('class', '');
    $('li[dir=' + filter + ']').attr('class', 'selected');
}

/**
 * Метод - действие контроллера реагирующий на события клики UI Pagination (пейджера)
 */
CThread.prototype.getThreadsForPagination = function()
{
	// Функциональные данные
    //TODO: проработать перенос обработки в рендеринг
    var id = this.data['id'];
    delete(this.data['id']);

    // Специализированные данные для запроса
    this.data['count'] = $.conf['PAGINATION_THREADS_PER_PAGE'];

    this._selfObj.getThreadsView();

    // TODO: проработать возможность переноса в класс представления Crendering со своим рендерингом
    // Изменения "фокуса" страницы в пейджере
   /* $('.current').attr('class', 'pagination');
    $('.pageList').find("a[id=" + id + "]").attr('class', 'current');*/
}