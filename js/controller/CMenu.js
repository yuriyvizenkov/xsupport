/**
 * Author: Greg
 * Date: 24.03.11
 * Класс контроллер, создающий управление для определённого действия к событию
 */

function CMenu(data)
{
    // Наследование свойств родительского класса Controller
    Controller.call(this, data);
    this.data['dispatch'] = 'Main';
}

// Наследование методов родительского класса
CMenu.prototype = inherit(Controller);

/**
 * Действие-метод производимое по умолчанию
 */
CMenu.prototype.Action = function()
{
    alert('Вызвано действие по умолчанию Action()');
}

CMenu.prototype.getThreadsForFilter = function()
{
    // Указание методов только в тестовых целях, названия методов будут в URL
    this.data['method'] = 'getListThreads';
    Controller.EVENT[this._idEvent]['request'] = {
        //url: '/index.php',
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if(modelData instanceof Error){
        return false;
    }

    this._selfObj.render('threads', {
        cache: this.cache,
        data: modelData
    });

    return true;
}