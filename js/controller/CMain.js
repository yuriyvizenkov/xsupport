/**
 * Author: Greg
 * Date: 24.03.11
 * Контроллер CMain с общими и глобальными действиями
 */

function CMain(data)
{
    // Наследование свойств родительского класса Controller
    Controller.call(this, data);
    this.data['dispatch'] = 'Main';
}
// Наследование методов родительского класса
CMain.prototype = inherit(Controller);

/**
 * Действие-метод производимое по умолчанию
 */
CMain.prototype.Action = function()
{
    alert('Вызвано действие по умолчанию Action()');
}

/**
 * Действие инициирует первоначальную загрузку меню в каркас приложения
 * В других действиях данные могут остутствовать this.data == ''
 */
CMain.prototype.setMainMenu = function()
{
    this.data['method'] = 'getMenu';
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData instanceof Error) {
        return false;
    }

    this._selfObj.render('menu', {
        data: modelData,
        cache: this.cache
    });

    return true;
}

/**
 * Метод обработки каркаса приложения
 */
CMain.prototype.setLayout = function()
{
    this.data['method'] = 'getLayout';
    // Фильтр по умолчанию
    this.data['selected_filter'] = 'myinbox';

    // Определение свойств события
     Controller.EVENT[this._idEvent]['dataEvent'] = {
        status: 'high',
        resets: true
    }

    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData == false) {
        return false;
    }

    this._selfObj.render('main', {
        cache: this.cache,
        data: modelData
    });
    // Для тестирования
    
    return true;
}

/**
 * Авторизация
 */
CMain.prototype.auth = function()
{
    this.data['action'] = 'auth';
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData == false) {
        return false;
    }

    $.conf['support'] = $.cookie('support');
    var main = $.mainLoad();

    return true;
}


/**
 * Событие инициирует подмену суппортера
 */
CMain.prototype.change_support = function()
{
    this.data['method'] = 'changeSupport';
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData instanceof Error) {
        return false;
    }

    this._selfObj.render('change_support', {
        data: modelData,
        cache: this.cache
    });

    return true;
}

/**
 * Вывод настроек пользователя на страницу
 */
CMain.prototype.settings = function()
{
    var modelData = {};
    this._selfObj.render('settings', {
        data: modelData,
        cache: this.cache
    });
    return true;
}

/**
 * Метод меняет настройки пользователя
 */
CMain.prototype.change_settings = function()
{
    this.data['method'] = 'change_settings';
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData instanceof Error) {
        return false;
    }

    return true;
}

/**
 * Метод инициирует смену проекта
 */
CMain.prototype.change_project = function()
{
    this.data['method'] = 'change_project';
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData instanceof Error) {
        return false;
    }

    return true;
}