/**
 * Author: Администратор
 * Date: 26.04.11
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function()
{
    // Авторизация
    $('#submit_auth').on("click", function(e)
    {
        $('#error').empty().removeClass('visible animated bounceIn');

	    var model = {
			    attributes: {
				    login: $('#login').val(),
				    password: $('#password').val()
			    }
		    },
		    options = {
				url: "login",
			    success: function(r) {
				    if (r.error === true) {
					    $('#error').empty().append(r.msg).addClass('visible animated bounceIn');
				    }
				    else {
					    $.app.redirect('/');
				    }
			    }
            };


	    $.app.sync("login", model, options);

	    e.preventDefault();
	    e.stopPropagation();
    });
});