$(document).ready(function() {

    $('.cb-ctrl-main').on('click', function() {
        var listActionsThreads = $(this).closest('.cb-ctrl').find('.cb-ctrl-slide-down-layer');
        if (listActionsThreads.hasClass('hidden')) {
            listActionsThreads.removeClass('hidden').addClass('visible animated flipInX');
        }
        else {
            listActionsThreads.removeClass('visible animated flipInX').addClass('hidden');
        }
    });

    $('.cb-ctrl-main').on('click_by_document', function(){
        var listActionsThreads = $(this).closest('.cb-ctrl').find('.cb-ctrl-slide-down-layer');
        listActionsThreads.removeClass('visible animated flipInX').addClass('hidden');
    });
});
