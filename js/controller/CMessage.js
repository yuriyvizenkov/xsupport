/**
 * Author: Greg
 * 16.03.2011
 * Контроллер, отражающий стратегию обработки событий для сообщений треда.
 */

/**
 * Конструктор класса контроллера CMesssage
 */
function CMessage(data)
{

    // Наследование свойств родительского класса Controller
    Controller.call(this, data);
    this.data['dispatch'] = 'Message';

}
// Наследование методов родительского класса
CMessage.prototype = inherit(Controller);

/**
 * Действие-метод производимое по умолчанию
 */
CMessage.prototype.Action = function()
{
    // TODO: Действие производимое по умолчанию, если нужно с сзапросом ajax (get, post, ajax)
    // Пример использования рендеринга. Функция должна будет использоваться как callback
    this.selfObj.render({
        data: dataRendering,
        cache: this.Request.cache
    });
}

/**
 * Метод управляет обработкой списка сообщений
 */
CMessage.prototype.getListMessages = function()
{
    $('#main-conteiner-loder-indicator-id').show()

    this.data['method'] = 'getListMessages';
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData instanceof Error) {
        return false;
    }

    this._selfObj.render('message', {
        data: modelData,
        cache: this.cache
    });

    return true;
}

/**
 * Метод управляет обработкой список спама
 */
CMessage.prototype.getListSpam = function()
{
    // Указание методов только в тестовых целях, названия методов будут в URL
    this.data['method'] = 'getListSpam';
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData instanceof Error) {
        return false;
    }

    this._selfObj.render('message', {
        data: modelData,
        cache: this.cache
    });

    return true;
}

/**
 * Метод позволяет закрыть тред
 */
CMessage.prototype.closeThread = function()
{
    this.data['method'] = 'closeThread';
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData instanceof Error) {
        return false;
    }

    this._selfObj.render('message', {
        cache: this.cache,
        data: modelData
    });
}

/**
 * Метод позволяет открыть тред
 */
CMessage.prototype.openThread = function()
{
    this.data['method'] = 'openThread';
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData instanceof Error) {
        return false;
    }

    this._selfObj.render('message', {
        cache: this.cache,
        data: modelData
    });
}

/**
 * Метод устанавливает сообщения обработанными
 */
CMessage.prototype.inProcessedMessage = function()
{
    var g_cortege_message = this.data['g_cortege_message'];
    delete(this.data['g_cortege_message']);
    
    this.data['method'] = 'inProcessed';
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData instanceof Error) {
        return false;
    }

    modelData['g_cortege_message'] = g_cortege_message;
    this._selfObj.render('inProcessed', {
        cache: this.cache,
        data: modelData
    });
}

/**
 * Метод управляет обработкой списка сообщений
 */
CMessage.prototype.addMessage = function()
{
    $('#main-conteiner-loder-indicator-id').show();

    var modelData = {'thread_id' : this.data['thread_id']};
    this._selfObj.render('add_message', {
        cache: this.cache,
        data: modelData
    });

    return true;
}

CMessage.prototype.delete_attachment = function()
{
    this.data['method'] = 'delete_attachment';
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData instanceof Error) {
        return false;
    }

    if(typeof modelData['attch_del'] != 'undefined')
    {
        return true;
    }

    return false;
}