/**
 * Author: Greg
 * Date: 23.03.11
 * Паттерн Composite
 * Класс компоновщик, совершает инициализацию контроллеров и запуск соответсвующих действий.
 * Упаковыввает результаты действий и возвращает в вызываемое событие.
 */ 
/**
 * Отладочная функция, аналог var_dump в PHP
 * @param obj
 */
function dump(obj) {
    var out = "";
    if(obj && typeof(obj) == "object"){
        for (var i in obj) {
            out += i + ": " + obj[i] + "\r\n";
        }
    } else {
        out = obj;
    }
    alert(out);
}

function htmlspecialchars(text)
{
   var chars = Array("&", "<", ">", '"', "'");
   var replacements = Array("&amp;", "&lt;", "&gt;", "&quot;", "'");
   for (var i=0; i<chars.length; i++)
   {
       var re = new RegExp(chars[i], "gi");
       if(re.test(text))
       {
           text = text.replace(re, replacements[i]);
       }
   }
   return text;
}

/**
 * Получение случайного ряда чисел
 *
 * @param min_random
 * @param max_random
 * @param countNumber
 */
function getRandom(min_random, max_random, countNumber)
{
    var id = '';
    for (i = 0; i < countNumber; i++) {
        var range = max_random - min_random + 1;
        id += String(Math.floor(Math.random() * range) + min_random);
    }
    return id;
}

/**
 * Конструктор класса компоновщика действий
 * @param Request
 */
function CompositeController(UIEvent) {

    /**
     * Объект класса UIEvent. Событие, которое инициализиировало компоновщик.
     * @param object
     */
    this.UIEvent = null;

    /**
     * Массив результатов работы контроллеров
     */
    this.results = new Array();

    // Если в формированиии события произошли ошибки
    if (UIEvent.error) {
        this.results = {
            error: new Error(UIEvent.error)
        }
    }

    this.UIEvent = UIEvent;
}

/**
 * Статический метод класса CompositeController
 * Пример даных передаваемых в функцию:
 * CompositeController.RUN_EVENT(
 * dataEvent : {
 *     status: 'high' // Приоритетность события
 * }
 * Controller: {
 *      cache: true;
 *      data: {data : data}, 
 *      action: 'getMainMenu'
 * });
 * @param object - Событие, передаваемое для обработки
 * @return array
 */
CompositeController.RUN_EVENT = function (event)
{
    //console.dir(event);
    if(typeof event.dataEvent != 'undefined')
    {
        // Инициализация компоновщика с определённым типом важности события
        var status = event.dataEvent['status'];
        if (status == 'high') {
            var UIEvent = new HighUIEvent(event);
            var composite = new CompositeController(UIEvent);
        }
    }
    else
    {
        var UIEvent = new NormalUIEvent(event);
        var composite = new CompositeController(UIEvent);
        // Дублирование, если в пользовательском вводе нет установленного статуса события
        var status = UIEvent.priority;
    }

    // Инициализация идентификатора события для любого вызываемого контроллера в пределах события
    var idEvent = UIEvent.ID_EVENT;
    Controller.SET_ID_EVENT(idEvent);

    // Инициализация контроллера
    var controllerName = 'C' + composite.UIEvent.controller;
    var controller = new window[controllerName]({
        nameController : controllerName,
        idEvent     : idEvent,
        statusEvent : status,
        resets      : composite.UIEvent.resets,
        action      : composite.UIEvent.action,
        dataRequest : composite.UIEvent.dataRequest,
        cache       : composite.UIEvent.cache
    });
    controller._selfObj = controller;

    // Запуск контроллера после проверки на инициализацию
    if (typeof controller != 'undefined') {
        var result = controller.run();

        if(!result){
            return false;
        }
        else {
            return true;
        }
    }
    else {
        alert('This controller: ' + controllerName +
              ' not instanceof ' + composite.controller.controllerName);
        return false;
    }

    return composite.results;
}

/**
 * Метод запускает процесс с учётом записи в "историю"
 * @param event
 */
CompositeController.RUN_EVENT_HISTORY = function (event)
{
    var url = CUrlManager.GET_URL_FROM_OBJECT(event);
    $.history.load(url);
    if($.conf['historyInit'] == false)
    {
        $.historyInit();
    }
}

/**
 * Класс событие из CommandsEvents.js
 * Так же класс является стандартным источником данных для контроллера
 *
 * Пример структуры события:
 *  dataEvent:{
 *      status: 'high'
 *  },
 *  Controller: {
 *      action: action,
 *      data: {data : data},
 *      cache: false
 *  }
 *
 * @param array
 */
function UIEvent(event) {

    /**
     * Название класса вызываемого контроллера
     * @param array
     */
    this.controller = new Array();

    /**
     * Флаг управления для установки кэширования
     * @param bool
     */
    this.cache = new Array();

    /**
     * Массив действий к соответствующему контроллеру из this.controllerName.
     * Если действие не указано для контроллера устанавливается false, что равнозначно действию по умолчанию action() в контроллере.
     * @param array
     */
    this.action = new Array();

    /**
     * Данные передаваемые в соответствующее действие из this.action.
     * Данных может и не быть.
     * @param array
     */
    this.dataRequest = new Array();

    /**
     * Идентификатор события
     * @type const
     */
    this.ID_EVENT = 'id_' + getRandom(0, 100, 3);

    /**
     * Ошибки пользовательского ввода
     * @param array
     */
    this.error = false;

    /**
     * Флаг сброса событий из очереди
     */
    this.resets = false;

    // Формируем событие
    for (var controller in event) {

        this.controller = controller;

        if (typeof event[controller].action != 'undefined') {
            this.action = event[controller].action;
        } else {
            this.error = {
                error: true,
                controllerName: controller,
                action: event[controller].action
            }
        }

        if (typeof event[controller].data != 'undefined') {
            this.dataRequest = event[controller].data;
        } else {
            this.dataRequest = false;
        }

        if (typeof event[controller].cache != 'undefined') {
            this.cache = event[controller].cache;
        } else {
            this.cache = true;
        }
    }
}

/**
 * Событие высокого приоритета
 * @param event
 */
function HighUIEvent(event) {

    /**
     * Приоритет важности события.
     * Используется менеджером запросов (AjaxManager).
     * @string
     */
    this.priority = 'high';
    UIEvent.call(this, event);
}

/**
 * Событие нормального приоритета
 * @param event
 */
function NormalUIEvent(event) {

    /**
     * Приоритет важности события.
     * Используется менеджером запросов (AjaxManager).
     * @string
     */
    this.priority = 'normal';
    UIEvent.call(this, event);
}