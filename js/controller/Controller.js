/**
 * Обработчик фоновых процессов открытых окон в виде стратегии.
 * Инкапсулирует в себе стратегию - алгоритм реакции определённых контроллеров и опреаций (actions)
 * Требуемые параметры от контроллера:
 * 1. action - вызываемый метод;
 * 2. nameController - имя инициализирующего контроллера;
 * @param controller object (class Controller)
 * @return bool
 */
function processedListOpenAnswers(controller)
{
    /**
     * Функция закрытия процессов с сохранением, взавиимости от параметров
     * @param bool
     */
    function _closeBackGroundProcess(before_close_save)
    {
        $.each(CRendering.LIST_OPEN_ANSWERS, function(index, value)
        {
            // Если требуется сохранение контента сообщений
            if(before_close_save == true)
            {
                value.clearProcessAnswerWithSave();
            }
            // Иначе останавливаем фоновые процессы окна ответа
            else
            {
                clearInterval(value.getIntervalID());
            }
        });

        CRendering.LIST_OPEN_ANSWERS = Array();
    }

    if(CRendering.LIST_OPEN_ANSWERS.length != 0)
    {
        //console.log(controller)
        // Если определённый набор контроллеров и операций выводим соответствующее
        // сообщение с сохранением контента из редакторов
        if( controller.nameController == 'CThread'
            || controller.action == 'change_support' || controller.action == 'settings')
        {
            // Выбор пользователя с последующей оперцией
            // Если пользователь отказывается от продолжения останавливаем процесс и возвращаем false
            if (!confirm("Существуют открытые ответы, сохранить?"))
            {
                return false;
            }
            
            _closeBackGroundProcess(true);
        }
        // Иначе, если другой набор условий закрываем фоновые процессы открытых окон и очищаем список открытых окон
        else if(controller.nameController == 'CAnswer' && controller.action == 'sendAnswer'
                && controller.data.typeAnswer != 'draft')
        {
            _closeBackGroundProcess(false);
        }
    }

    return true;
}

/**
 * Реализация наследования
 * @param object
 * @return object
 */
function inherit(object)
{
    function temp()
    {
    }

    temp.prototype = object.prototype;
    return (new temp());
}

/**
 * Статическое свойство для всех классов контроллеров.
 * Структура события:
 * {
 *     nameController : 'CMain',
 *     idEvent: 'id_65489841330',
 *     status: 'high',
 *     resets: true,
 *     request: {
 *              url: 'support_desktop/getCountMessage/',
 *              data: {
 *                  "id_thread" : "256",
 *                  "support" : "support"
 *              }
 *         }
 * }
 */
Controller.EVENT = new Array();

/**
 * Author: Greg
 * Date: 23.03.11
 * Класс реализующий общие параметры-свойства и методы для всех дочерних подклассов
 */
function Controller(data)
{

    /**
     * Экземпляр, инициализированного в компоновщике, контроллера 'XController' для использования в prototype функциях
     * @var object
     */
    this._selfObj = null;

    /**
     * Идентификатор события
     * @var int
     */
    this._idEvent = data.idEvent;

    /**
     * Имя вызывающего контроллера
     * @var string
     */
    this.nameController = data.nameController;

    /**
     * Вызываемое действие
     * @var string
     */
    this.action = data.action;

    /**
     * Данные для действия
     * @var object
     */
    this.data = data.dataRequest;

    /**
     * Использование кэша
     * @var bool
     */
    this.cache = data.cache;

    /**
     * Действие является последним
     * Deprecated
     * @var bool
     */
    this.lastAction = false;

    /**
     * Приоритет события
     * 'low', 'normal', 'hight'
     * @var string
     */
    this.statusEvent = data.statusEvent;

    /**
     * Право на сброс очереди событий
     * @var bool
     */
    this.resets = data.resets;

    // Deprecated
    if (data.lastAction) {
        this.lastAction = true;
    }
}

/**
 * Метод является интерфейсом инициализации всего проецасса.
 */
Controller.prototype.run = function ()
{
    //var action = this.action;
    var result = null;

    if (this._selfObj.beforeAction() == false)
    {
        return false;
    }

    // Если действие по умолчанию action = false
    if (!this.action)
    {
        result = this._selfObj.Action();
        this._selfObj.afterAction();
        return result;
    }

    if (typeof this._selfObj[this.action] == 'function')
    {
        // Вызов метода определённого действия из события. Название действия хранится в Controller.action
        var resAction = this._selfObj[this.action]();
        this._selfObj.afterAction();

        if (resAction) {
            return true;
        } else {
            return result;
        }

    }
    else
    {
        alert('Its ' + this._selfObj[this.action] + ' not function!');
        return false;
    }
}

/**
 * "Статический" метод - интерфейс для класса Controller.
 * Устанавливает уникальный идентификатор для события и используется в очереди ajax запросов
 * @param int
 * @return bool
 */
Controller.SET_ID_EVENT = function(idEvent)
{
    if (typeof idEvent == 'undefined' || idEvent == '') {
        alert('idEvent from class Controller not defined!');
        return false;
    }
    Controller.EVENT[idEvent] = new Array();
    return true;
}

/**
 * Метод вызываемый перед началом основного действия (action)
 * Организовывает валидацию и инициализацию данных события
 * @return bool
 */
Controller.prototype.beforeAction = function()
{
    // Если идентификатор события не определён
    if (this._idEvent == 'undefined') {
        alert('idEvent not defined!!!');
        return false;
    }
    // Определяется для CAjaxManager
    Controller.EVENT[this._idEvent]['idEvent'] = this._idEvent;

    if (typeof this.statusEvent != 'undefined') {
        Controller.EVENT[this._idEvent]['status'] = this.statusEvent;
    }

    if (typeof this.resets != 'undefined') {
        Controller.EVENT[this._idEvent]['resets'] = this.resets;
    }

    if (typeof this.data == 'undefined' || this.data == false) {
        this.data = '';
    }

    // Если контроллеры не ответа и работы со списком собщений, проверяем существование открытых окон ответов
    // Если такие находятся выводим сообщение о выборе смотри обработчик processedListOpenAnswer
    var by_continue = processedListOpenAnswers(this);
    if(by_continue == false)
    {
        return false;
    }

    return true;
}

/**
 * Метод вызываемый по завершению основного действия
 */
Controller.prototype.afterAction = function()
{
    // TODO: перед удалением есть возможность создать историю состояния
    delete(Controller.EVENT[this._idEvent]);
}

/**
 * Метод реализующий управление обработкой запросов и передача управления рендерингу
 * @return object|bool
 */
Controller.prototype.ajaxRequest = function ()
{
    var ajax = new CAjaxManager(this._idEvent);
    var resultAddQueue = ajax.addQueue(Controller.EVENT[this._idEvent]);
    console.time("timing ajax_request");
    // Получаем результат ответов от сервера
    var dataRendering = ajax.runAjaxManager();
    console.timeEnd("timing ajax_request");
    //console.log(dataRendering)
    if (typeof dataRendering['result'] != 'undefined' || dataRendering['result'] == 'error') {

        // Вывод exception
        if (dataRendering['action'] != 'auth')
        {
            if(typeof dataRendering['msg']['xdebug_message'] != 'undefined')
                dump(dataRendering['msg']['xdebug_message'].substr(0, 255));
            else
                dump(dataRendering['msg']);
            //console.log(dataRendering)
        } else {
            // Оповещение об необходимости авторизации (форма авторизации)
            this._selfObj.render('auth', {
                data : dataRendering,
                cache: this.cache
            });
        }
        return false;
    }

    return dataRendering;
}

/**
 * Рендеринг результатов запроса
 * Формат передаваемых данных пример:
 * [ {text: "Request it`s sucessfuly!"} ]
 * @param method string Метод для рендеринга
 * @param data object Данные, возврщаемые сервером (JSON)
 */
Controller.prototype.render = function(method, data)
{
    var rendering = new CRendering(data);
    rendering._selfObj = rendering;
    rendering.run(method);
}
