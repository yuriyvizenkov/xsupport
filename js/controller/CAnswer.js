/**
 * Author: Greg
 * 16.03.2011
 * Контроллер, отражающий стратегию обработки событий для сообщений треда.
 */

/**
 * Конструктор класса контроллера CMesssage
 */
function CAnswer(data)
{

    // Наследование свойств родительского класса Controller
    Controller.call(this, data);
    this.data['dispatch'] = 'Answer';

}
// Наследование методов родительского класса
CAnswer.prototype = inherit(Controller);

/**
 * Действие-метод производимое по умолчанию
 */
CAnswer.prototype.Action = function()
{
    // TODO: Действие производимое по умолчанию, если нужно с сзапросом ajax (get, post, ajax)
    // Пример использования рендеринга. Функция должна будет использоваться как callback
    this.selfObj.render({
        data: dataRendering,
        cache: this.Request.cache
    });
}

/**
 * Метод управляет контекстами действий "Ответить" и "Редактировать"
 */
CAnswer.prototype.answer = function()
{
    // Данные требуемые для рендеринга
    var modelData = Array();

    // Выгружаем и удаляем данные из JSON - передачи параметров на сервер
    var renderTo = this.data['r'];
    delete(this.data['r']);
    var edit = this.data['edit'];

    this.data['method'] = 'getPossibleMessage';

    // Ставим в запрос пакет данных
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    // Получаем данные от сервера посредством ajax manager
    modelData = this._selfObj.ajaxRequest();
    if (modelData instanceof Error) {
        return false;
    }

    modelData['renderTo'] = renderTo;
    modelData['edit'] = edit;
    this._selfObj.render('answer', {
        data: modelData,
        cache: this.cache
    });

    // Если событие контекста "Ответить" обновляем меню
    // событие контекста "Редактировать" не обновляет меню, так как оно не создаёт черновика,
    // а является так же черновиком
    if(this.data['edit'] != 'edit')
    {
        this._selfObj.render('menu', {
            data: modelData,
            cache: this.cache
        });
    }
    
    return true;
}

/**
 * Метод управляет логикой работы операции смены слоя в окне ответа
 */
CAnswer.prototype.changeLayer = function()
{
    // Объект окна ответа для использования в рендеринге
    var ctrlAnswer = this.data['ctrlAnswer'];
    delete(this.data['ctrlAnswer']);

    this.data['method'] = 'changeLayer';
    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData instanceof Error) {
        return false;
    }

    modelData['ctrlAnswer'] = ctrlAnswer;
    this._selfObj.render('changeLayer', {
        data: modelData,
        cache: this.cache
    });
    return true;
}

/**
 * Метод управляет событиями "Отправить", "В черновик" и "Удалить"
 */
CAnswer.prototype.sendAnswer = function()
{
    // Объект окна ответа для использования в рендеринге
    var ctrlAnswer = this.data['ctrlAnswer'];
    delete(this.data['ctrlAnswer']);

    // Если определён тип запроса как del_draft вызываем метод удаления черновика
    // иначе сохранение или черновика или ответа на сообщение
    if (this.data['typeAnswer'] == 'del_draft') {
        this.data['method'] = 'deleteDraft';
    } else {
        this.data['method'] = 'reply';
    }

    Controller.EVENT[this._idEvent]['request'] = {
        data: this.data
    }

    var modelData = this._selfObj.ajaxRequest();
    if (modelData instanceof Error) {
        return false;
    }

    modelData['ctrlAnswer'] = ctrlAnswer;

    // Обновляем список сообщений только в том случае, если тип действия "Отправить" или "Удалить сообщение"
    if ((this.data['typeAnswer'] == 'send' || this.data['typeAnswer'] == 'del_draft')) {
        this._selfObj.render('message', {
            data: modelData,
            cache: this.cache
        });
    }

    // Если выбрана операция удаления черновика следует обновить меню
    if (this.data['typeAnswer'] == 'del_draft' || ctrlAnswer['type_answer'] == 'new_thread') {
        // Обновляем меню
        this._selfObj.render('menu', {
            data: modelData,
            cache: this.cache
        });
    }

    return true;
}


